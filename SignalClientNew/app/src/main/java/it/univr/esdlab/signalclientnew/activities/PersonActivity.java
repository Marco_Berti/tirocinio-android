package it.univr.esdlab.signalclientnew.activities;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import it.univr.esdlab.signalclientnew.activities.person.AddPersonFragment;
import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.activities.person.SessionDetailFragment;
import it.univr.esdlab.signalclientnew.activities.person.PersonDetailFragment;
import it.univr.esdlab.signalclientnew.activities.person.PersonFragment;
import it.univr.esdlab.signalclientnew.datamapper.DataMapper;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.model.Session;
import it.univr.esdlab.signalclientnew.singletones.DBPersonToPerson;
import it.univr.esdlab.signalclientnew.singletones.DBSessionToSession;
import it.univr.esdlab.signalclientnew.singletones.DataStorage;

public class PersonActivity extends AppCompatActivity {

    private static final String TAG = "PersonActivity";
    private IntentFilter filter;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                Log.e(TAG, "CONNESSIONE CHIUSA - socket connected: " + DataStorage.getInstance().getSocket());
                DataStorage.getInstance().setSocket(null);
                Log.e(TAG, "CONNESSIONE CHIUSA - socket connected: " + DataStorage.getInstance().getSocket());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_person);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DataMapper.getInstance().setContext(getApplicationContext());
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new PersonFragment()).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
        filter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiver,filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
        unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_persons, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void startAddFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = new AddPersonFragment();
        ft.replace(R.id.fragment, fragment);
        ft.addToBackStack(null);
        ft.commit();

    }

    public void popSavedFragment() {
        getSupportFragmentManager().popBackStack();
    }

    public void openSessions(int personId) {
        Person person = DBPersonToPerson.getInstance().getPerson(personId);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = PersonDetailFragment.newInstance(person);
        ft.replace(R.id.fragment, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void startSendActivity(Person person) {
        Intent intent =  new Intent(getApplicationContext(), SendActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("PERSON", person.getId());
        //bundle.putSerializable("PERSON", person);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * Fragment che mostra i dettagli della sessione
     * @param sessionId id della sessione
     */
    public void startDetailFragment(int sessionId) {
        Session session = DBSessionToSession.getInstance().getSessionDetail(sessionId);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = SessionDetailFragment.newInstance(session);
        ft.replace(R.id.fragment, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}
