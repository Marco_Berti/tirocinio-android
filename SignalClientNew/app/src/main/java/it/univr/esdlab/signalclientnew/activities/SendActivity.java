package it.univr.esdlab.signalclientnew.activities;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.activities.send.ConnectionFragment;
import it.univr.esdlab.signalclientnew.activities.send.SendFragment;
import it.univr.esdlab.signalclientnew.activities.send.SendSettingsFragment;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.singletones.DBPersonToPerson;
import it.univr.esdlab.signalclientnew.singletones.DataStorage;

public class SendActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int ACCESS_COARSE_LOCATION = 99;
    private static final String TAG = "SendActivity";
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private ConnectionFragment connectionFragment = null;
    private Person mPerson;
    private IntentFilter filter;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceive");
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                Toast.makeText(getApplicationContext(), "Discovery started...", Toast.LENGTH_SHORT).show();
            }
            else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                connectionFragment.addDevices(device);
                //Toast.makeText(getApplicationContext(), "Device found: " + device.getName(), Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Device found: " + device.getName() + "; MAC " + device.getAddress());
            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Toast.makeText(getApplicationContext(), "Discovery finished", Toast.LENGTH_SHORT).show();
                connectionFragment.setSearchProgressBarVisibility(false);
                if(connectionFragment.getSearchButtonText().equals("Stop"))
                    connectionFragment.setSearchButtonText("Paired Devices");
            }
            else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action) && DataStorage.getInstance().getSocket() != null
                    && device.getAddress().equals((DataStorage.getInstance().getSocket().getRemoteDevice().getAddress()))) {
                Toast.makeText(getApplicationContext(), "CONNECTED TO " + device.getName(), Toast.LENGTH_SHORT).show();
                //connectionFragment.deviceConnected(mBluetoothSocket);
                //mDialog.dismiss();
            }
            else if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                Log.e(TAG, "CONNESSIONE CHIUSA - socket connected: " + DataStorage.getInstance().getSocket());
                Toast.makeText(getApplicationContext(), "DISCONNECTED FROM " + device.getName(), Toast.LENGTH_SHORT).show();
                //connectionFragment.setDisconnectProgressBarVisibility(false);
                //connectionFragment.deviceDisconnected();
                DataStorage.getInstance().setSocket(null);
                Log.e(TAG, "CONNESSIONE CHIUSA - socket connected: " + DataStorage.getInstance().getSocket());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        setContentView(R.layout.activity_send);

        Bundle extraBundle = getIntent().getExtras();
        if(!extraBundle.isEmpty() && extraBundle.containsKey("PERSON")) {
            // mPerson = (Person) extraBundle.getSerializable("PERSON");
            int id = extraBundle.getInt("PERSON");
            mPerson = DBPersonToPerson.getInstance().getPerson(id);
        }

        if(connectionFragment == null) {
            connectionFragment = new ConnectionFragment();
            Log.d(TAG, "connection fragment creato");
        }

        if(DataStorage.getInstance().getSocket() != null && DataStorage.getInstance().getSocket().isConnected()) {
            //lancio fragment di send
            Log.e(TAG, "lancio fragment di invio");
            callSendSettingsFragment();
            Log.d(TAG, "socket:" + DataStorage.getInstance().getSocket().toString() + " connected:" + DataStorage.getInstance().getSocket().isConnected());
        }
        else {
            //lancio fragment di connessione
            Log.e(TAG, "lancio fragment di connessione");
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_place, connectionFragment).commit();
        }
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "onStart");
        super.onStart();
        filter  = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        if(DataStorage.getInstance().getSocket() == null)
            initialize();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        registerReceiver(mReceiver,filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        unregisterReceiver(mReceiver);
    }

    public void initialize() {

        if (mBluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            Toast.makeText(this, "Bluetooth is not supported", Toast.LENGTH_SHORT).show();
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        if (DataStorage.getInstance().getSocket() == null && connectionFragment.getSearchButtonText().equals("Search"))
            pairedDevices();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK)
            pairedDevices();
        else
            Toast.makeText(this, "You have to enable bluetooth", Toast.LENGTH_SHORT).show();
    }

    public boolean checkLocationPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        else {
            // Permission has already been granted
            return true;
        }
    }


    public void requestLocationPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                ACCESS_COARSE_LOCATION);

        // ACCESS_COARSE_LOCATION is an
        // app-defined int constant. The callback method gets the
        // result of the request.

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ACCESS_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    //Toast.makeText(this, "LOCATION GRANTED", Toast.LENGTH_SHORT).show();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "You have to allow access to position!", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public void pairedDevices() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            connectionFragment.setDevices(new ArrayList<>(pairedDevices));
            Log.d(TAG, "paired devices: " + pairedDevices);
        } else{
            Toast.makeText(this, "No paired devices, please search", Toast.LENGTH_SHORT).show();
            connectionFragment.setDevices(new ArrayList<>());

        }
    }

    public void searchDevices() {
        if (!checkLocationPermissions())
            requestLocationPermissions();
        //Start discovery
        mBluetoothAdapter.startDiscovery();
        // Register for broadcasts when a device is discovered.
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

    public void sendSignalMessage(String type, int duration, int intervalsDuration, String manual) {
        String message = type + "," + duration + "," + intervalsDuration + "," + manual;
        DataStorage.getInstance().getThread().write(message.getBytes());
    }

    public void sendSimpleMessage(String message) {
        DataStorage.getInstance().getThread().write(message.getBytes());
    }

    public void callSendSettingsFragment() {
        //mando al fragment di che persona sto trattando
        Log.d(TAG, "invoco nuovo fragment : " + mPerson);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = SendSettingsFragment.newInstance(mPerson);
        ft.replace(R.id.fragment_place, fragment).commit();
    }

    public void callSendFragment(String mod, int sessionId, boolean intervals,
                                 int intervalsDuration, boolean duration, int pulseDuration, boolean vibration,
                                 boolean sound) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = SendFragment.newInstance(mod, mPerson.getId(), sessionId, intervals, intervalsDuration, duration,
                pulseDuration, vibration, sound);
        ft.replace(R.id.fragment_place, fragment).commit();
    }

    public BluetoothAdapter getBluetoothAdapter() {
        return mBluetoothAdapter;
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "onBackPressed");

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_place);
        if(f instanceof SendFragment) {
            callDialog();
        }
        else
            super.onBackPressed();

    }

    public void callDialog() {
        Log.d(TAG, "DIALOG CHIAMATO");
        Toast.makeText(this, "You are performing a session, please stop it.", Toast.LENGTH_SHORT).show();
    }
}
