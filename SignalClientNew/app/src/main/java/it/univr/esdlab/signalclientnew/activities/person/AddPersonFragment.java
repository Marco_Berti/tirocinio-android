package it.univr.esdlab.signalclientnew.activities.person;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.activities.PersonActivity;
import it.univr.esdlab.signalclientnew.singletones.DBPersonToPerson;


public class AddPersonFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "AddPersonFragment";

    private Calendar mCalendar = Calendar.getInstance();
    private ClearableEditText mName;
    private ClearableEditText mDateText;
    private ClearableEditText mBirthplace;
    private ClearableEditText mSickSince;
    private RadioGroup mGenre;
    private Button mCancelButton;
    private Button mSaveButton;

    private String mParam1;
    private String mParam2;

    public AddPersonFragment() {
        // Required empty public constructor
    }

    public static AddPersonFragment newInstance(String param1, String param2) {
        AddPersonFragment fragment = new AddPersonFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    DatePickerDialog.OnDateSetListener date = (view, year, month, dayOfMonth) -> {
        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateDate();

    };

    DatePickerDialog.OnDateSetListener sick = (view, year, month, dayOfMonth) -> {
        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateSick();
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_person, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mName = view.findViewById(R.id.patient_name);

        mDateText = view.findViewById(R.id.patient_birthday);
        mDateText.setKeyListener(null);
        //mDateText.setEnabled(false);
        mDateText.setOnClickListener(v -> dateTextOnClick());

        mBirthplace = view.findViewById(R.id.patient_birthplace);

        mSickSince = view.findViewById(R.id.patient_sick_since);
        mSickSince.setKeyListener(null);
        mSickSince.setOnClickListener(v -> sickSinceOnClick());

        mGenre = view.findViewById(R.id.genre_group);
        mGenre.check(R.id.male_radio_button);

        mCancelButton = view.findViewById(R.id.cancel_button);
        mCancelButton.setOnClickListener(v -> cancelOnClick());

        mSaveButton = view.findViewById(R.id.save_button);
        mSaveButton.setOnClickListener(v -> saveOnClick());
    }

    public Calendar stringToCalendar(String s) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
        try {
            cal.setTime(sdf.parse(s));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    private void saveOnClick() {
        RadioButton r = getView().findViewById(mGenre.getCheckedRadioButtonId());
        DBPersonToPerson.getInstance().createPerson(mName.getText().toString(), stringToCalendar(mDateText.getText().toString()),
                mBirthplace.getText().toString(), stringToCalendar(mSickSince.getText().toString()), r.getText().toString());
        ((PersonActivity) getActivity()).popSavedFragment();
    }

    private void cancelOnClick() {
        ((PersonActivity) getActivity()).popSavedFragment();
    }

    private void sickSinceOnClick() {
        if (!mDateText.getText().toString().equals("")) {
            DatePickerDialog dpd = new DatePickerDialog(getContext(), /*AlertDialog.THEME_HOLO_LIGHT,*/ sick, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
            dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
            dpd.getDatePicker().setMinDate(stringToCalendar(mDateText.getText().toString()).getTimeInMillis());
            dpd.show();
        }
        else
            Toast.makeText(getContext(), "You must insert birthday before", Toast.LENGTH_SHORT).show();
    }

    private void dateTextOnClick() {
        DatePickerDialog dpd = new DatePickerDialog(getContext(), /*AlertDialog.THEME_HOLO_LIGHT,*/ date, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.show();
    }

    private void updateDate() {
        String format = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ITALY);
        mDateText.setText(sdf.format(mCalendar.getTime()));
    }

    private void updateSick() {
        String format = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ITALY);
        mSickSince.setText(sdf.format(mCalendar.getTime()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
