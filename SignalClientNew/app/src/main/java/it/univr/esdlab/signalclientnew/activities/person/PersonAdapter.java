package it.univr.esdlab.signalclientnew.activities.person;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import java.util.TreeSet;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.model.PersonSmall;
import it.univr.esdlab.signalclientnew.model.SessionSmall;

public class PersonAdapter extends RecyclerView.Adapter<PersonHolder> {

    private List<PersonSmall> mValues;
    private PersonFragment owner;

    public PersonAdapter(List<PersonSmall> items, PersonFragment owner) {
        this.owner = owner;
        mValues = items;
        notifyDataSetChanged();
    }

    @Override
    public PersonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_person, parent, false);
        return new PersonHolder(view, this);
    }

    @Override
    public void onBindViewHolder(PersonHolder holder, int position) {
        PersonSmall item = mValues.get(position);
        holder.setPerson(item);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void openSession(int personID) {
        owner.openSessions(personID);
    }

    public void updateValues(List<PersonSmall> personList) {
        mValues.clear();
        mValues.addAll(personList);
        notifyDataSetChanged();
    }

}

