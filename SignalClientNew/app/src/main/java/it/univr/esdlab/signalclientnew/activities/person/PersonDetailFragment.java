package it.univr.esdlab.signalclientnew.activities.person;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.activities.PersonActivity;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.model.PersonSmall;
import it.univr.esdlab.signalclientnew.model.Session;
import it.univr.esdlab.signalclientnew.model.SessionSmall;
import it.univr.esdlab.signalclientnew.singletones.DBPersonToPerson;


public class PersonDetailFragment extends Fragment implements Person.OnChangeListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PERSON = "Person";
    //private static final String ARG_PARAM2 = "param2";

    private static final  String TAG = "PersonDetailFragment";
    private Person mPerson;
    private TextView mId;
    private TextView mName;
    private TextView mAge;
    private TextView mSickSince;
    private TextView mBirthplace;
    private TextView mGenre;
    private RecyclerView mSessions;
    private FloatingActionButton fab;
    private SessionAdapter mSessionAdapter;

    public PersonDetailFragment() {
        // Required empty public constructor
    }

    public static PersonDetailFragment newInstance(Person p) {
        PersonDetailFragment fragment = new PersonDetailFragment();
        Bundle args = new Bundle();
        args.putInt(PERSON, p.getId());
        // args.putSerializable(PERSON, p);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            int id = getArguments().getInt(PERSON);
            mPerson = DBPersonToPerson.getInstance().getPerson(id);

            //mPerson = (Person) getArguments().getSerializable(PERSON);
            //assert mPerson != null;
            mPerson.setOnChangeListener(this);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);

        mId = view.findViewById(R.id.item_number);
        mId.setText("" + mPerson.getId());

        mName = view.findViewById(R.id.name);
        mName.setText(mPerson.getName());

        mAge = view.findViewById(R.id.age);
        mAge.setText(mPerson.getAge());

        mBirthplace = view.findViewById(R.id.patient_birthplace);
        mBirthplace.setText(mPerson.getBirthplace());

        mGenre = view.findViewById(R.id.patient_genre);
        mGenre.setText(mPerson.getGenre());

        mSickSince = view.findViewById(R.id.patient_sick_since);
        mSickSince.setText(calendarToString(mPerson.getSickSince()));

        mSessions = view.findViewById(R.id.session_listView);
        mSessions.setLayoutManager(new LinearLayoutManager(getActivity()));
        mSessions.setAdapter(mSessionAdapter = new SessionAdapter(new ArrayList<>(mPerson.getSessionsList()),this));
        //mSessionAdapter = new SessionAdapter(mPerson.getSessionsList(), this);

        fab = view.findViewById(R.id.add_floating_button);
        fab.setOnClickListener(v -> fabOnClickListener());
    }

    public String calendarToString(Calendar calendar) {
        String format = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ITALY);
        return sdf.format(calendar.getTime());

    }

    private void fabOnClickListener() {
        ((PersonActivity)getActivity()).startSendActivity(mPerson);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        mSessionAdapter.updateValues(mPerson.getSessionsList());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void changeFragment(int sessionId) {
        ((PersonActivity)getActivity()).startDetailFragment(sessionId);
    }

    @Override
    public void onPersonChanged(Person person) {
        Log.d(TAG, "onPersonChanged");
        this.mPerson = person;
    }
}