package it.univr.esdlab.signalclientnew.activities.person;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.couchbase.lite.Database;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.activities.PersonActivity;
import it.univr.esdlab.signalclientnew.datamapper.DataMapper;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.singletones.DBPersonToPerson;
import it.univr.esdlab.signalclientnew.singletones.DBSessionToSession;

public class PersonFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private FloatingActionButton addFAB;
    private PersonAdapter mPersonAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PersonFragment() {
    }

    @SuppressWarnings("unused")
    public static PersonFragment newInstance(int columnCount) {
        PersonFragment fragment = new PersonFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_person_list, container, false);
        //RecyclerView mRecyclerView = view.findViewById(R.id.list);

        // Set the adapter
        if (view.findViewById(R.id.list) instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = view.findViewById(R.id.list);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            mPersonAdapter = new PersonAdapter(DBPersonToPerson.getInstance().getPersonsSmallList(), this);
            recyclerView.setAdapter(mPersonAdapter);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addFAB = view.findViewById(R.id.add_floating_button);
        addFAB.setOnClickListener(v -> addFABOnclickListener());
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this::startPullReplication);
    }

    private void startPullReplication(){
        DataMapper.getInstance().startPullReplication(this);
    }

    public void stopRefreshing(){
        mSwipeRefreshLayout.setRefreshing(false);
        mPersonAdapter.updateValues(DBPersonToPerson.getInstance().getPersonsSmallList());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPersonAdapter.notifyDataSetChanged();
    }

    private void addFABOnclickListener() {
        ((PersonActivity)getActivity()).startAddFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void openSessions(int personId) {
        ((PersonActivity)getActivity()).openSessions(personId);
    }
}