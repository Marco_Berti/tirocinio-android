package it.univr.esdlab.signalclientnew.activities.person;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.model.PersonSmall;

public class PersonHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private static final String TAG = "Person Holder";
    private PersonAdapter owner;
    private PersonSmall mPersonSmall;
    private TextView mName;
    private TextView mId;
    private TextView mAge;

    public PersonHolder(View itemView, PersonAdapter owner) {
        super(itemView);
        this.owner = owner;
        mName = itemView.findViewById(R.id.name);
        mId = itemView.findViewById(R.id.item_number);
        mAge = itemView.findViewById(R.id.age);
        //Log.d(TAG, itemView + "/" + mName + "/" + mId + "/" + mAge );
        itemView.setOnClickListener(v -> onClick(v));
    }

    public void setPerson(PersonSmall person) {
        mPersonSmall = person;
        mId.setText("" + mPersonSmall.getId());
        mName.setText(mPersonSmall.getName());
        mAge.setText(mPersonSmall.getAge());
    }


    @Override
    public void onClick(View v) {
        owner.openSession(mPersonSmall.getId());
    }
}
