package it.univr.esdlab.signalclientnew.activities.person;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.TreeSet;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.model.Session;
import it.univr.esdlab.signalclientnew.model.SessionSmall;

public class SessionAdapter extends RecyclerView.Adapter<SessionHolder> {

    private List<SessionSmall> mValues;
    private PersonDetailFragment owner;

    public SessionAdapter(List<SessionSmall> items, PersonDetailFragment owner) {
        this.owner = owner;
        mValues = items;
        // notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SessionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.session_info, parent, false);
        return new SessionHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull SessionHolder holder, int position) {
        SessionSmall item = mValues.get(position);
        holder.setSession(item);
        //holder.getFreezeNumber().setText(item.getFreeze());
        //holder.getTimestamp().setText(item.getStartTimeStamp().toString());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void changeFragment(SessionSmall session) {
        owner.changeFragment(session.getId());
    }

    public void updateValues(TreeSet<SessionSmall> sessionsList) {
        mValues.clear();
        mValues.addAll(sessionsList);
        notifyDataSetChanged();
    }
}
