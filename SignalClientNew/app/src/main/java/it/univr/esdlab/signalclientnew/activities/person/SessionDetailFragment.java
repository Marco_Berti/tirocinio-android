package it.univr.esdlab.signalclientnew.activities.person;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.model.Session;

public class SessionDetailFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String SESSION = "Session";
    //private static final String ARG_PARAM2 = "param2";

    private Session mSession;
    private TextView mTimestamp;
    private TextView mType;
    private TextView mDuration;
    private TextView mIntervals;
    private TextView mFreeze;
    private TextView mNotes;


    public SessionDetailFragment() {
        // Required empty public constructor
    }

    public static SessionDetailFragment newInstance(Session s) {
        SessionDetailFragment fragment = new SessionDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(SESSION, s);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSession = (Session) getArguments().getSerializable(SESSION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_session_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTimestamp = view.findViewById(R.id.detail_timestamp);
        mTimestamp.setText(mSession.getStartTimeStamp().toString());

        mType = view.findViewById(R.id.detail_type);
        mType.setText(mSession.getType());

        mDuration = view.findViewById(R.id.detail_duration);
        mDuration.setText("" + mSession.getDuration());

        mIntervals = view.findViewById(R.id.detail_intervals);
        mIntervals.setText("" + mSession.getInterval());

        mFreeze = view.findViewById(R.id.detail_freeze_number);
        mFreeze.setText("" + mSession.getFreeze());

        mNotes = view.findViewById(R.id.detail_notes);
        mNotes.setText("" + mSession.getNotes());

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
