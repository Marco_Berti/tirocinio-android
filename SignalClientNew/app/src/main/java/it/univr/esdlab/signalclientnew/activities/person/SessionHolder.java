package it.univr.esdlab.signalclientnew.activities.person;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.model.Session;
import it.univr.esdlab.signalclientnew.model.SessionSmall;

public class SessionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static final String TAG = "SessionHolder";
    private SessionAdapter owner;
    private SessionSmall mSession;
    private TextView mTimestamp;
    private TextView mFreezeNumber;

    public SessionHolder(View itemView, SessionAdapter owner) {
        super(itemView);
        this.owner = owner;
        mTimestamp = itemView.findViewById(R.id.timestamp);
        mFreezeNumber = itemView.findViewById(R.id.freeze_number);
        itemView.setOnClickListener(this);
    }

    public void setSession(SessionSmall session) {
        mSession = session;
        mTimestamp.setText(mSession.getStartTimestamp().toString());
        mFreezeNumber.setText("" + mSession.getFreeze());
        //notifyAll();
    }

    public TextView getTimestamp() {
        return mTimestamp;
    }

    public TextView getFreezeNumber() {
        return mFreezeNumber;
    }

    @Override
    public void onClick(View v) {
        owner.changeFragment(mSession);
        Log.d(TAG, "HAI PREMUTO UNA COSA INVISIBILE");
    }
}
