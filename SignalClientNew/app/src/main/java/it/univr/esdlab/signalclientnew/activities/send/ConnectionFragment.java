package it.univr.esdlab.signalclientnew.activities.send;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.activities.SendActivity;
import it.univr.esdlab.signalclientnew.singletones.DataStorage;


public class ConnectionFragment extends Fragment {

    private static final String TAG = "Connection Fragment";
    private static final String KEY_STATUS = "status";
    private List<BluetoothDevice> mDevices = new ArrayList<>();
    private DeviceAdapter mDeviceAdapter;
    //private BluetoothDevice d;
    private TextView mDeviceNameView;
    private TextView mDeviceAddressView;
    private TextView mStatus;
    private Button mDisconnectButton;
    private ProgressBar mDisconnectProgressBar;
    private ProgressBar mSearchProgressBar;
    private RecyclerView mDevicesRecyclerView;
    private Button mSearchButton;
    private boolean mConnected;

    /*public void setD(int index) {
        d = mDevices.get(index);
        //Toast.makeText(getContext(), d.getAddress(), Toast.LENGTH_SHORT).show();

    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(TAG, "onCreatedView");
        return inflater.inflate(R.layout.fragment_connection, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");


        mDeviceNameView = view.findViewById(R.id.device_name);
        mDeviceNameView.setText("null");

        mDeviceAddressView = view.findViewById(R.id.device_address);
        mDeviceAddressView.setText("null");

        mStatus = view.findViewById(R.id.status);
        mStatus.setText("DISCONNECTED");
        mStatus.setTextColor(getResources().getColor(R.color.colorDisconnected));

        mDisconnectButton = view.findViewById(R.id.disconnect_button);
        mDisconnectButton.setVisibility(View.INVISIBLE);
        mDisconnectButton.setOnClickListener(v -> mDisconnectOnClick());

        mDisconnectProgressBar = view.findViewById(R.id.disconnect_progress_bar);
        mDisconnectProgressBar.setIndeterminate(true);
        mDisconnectProgressBar.setVisibility(View.INVISIBLE);

        mSearchProgressBar = view.findViewById(R.id.search_progress_bar);
        mSearchProgressBar.setIndeterminate(true);
        mSearchProgressBar.setVisibility(View.INVISIBLE);

        mDevicesRecyclerView = view.findViewById(R.id.devices_recycler_view);
        mDevicesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDevicesRecyclerView.setAdapter(mDeviceAdapter = new DeviceAdapter(mDevices, this));

        System.out.println("ON CREATE VIEW");
        mSearchButton = view.findViewById(R.id.not_listed_button);
        mSearchButton.setOnClickListener(v -> mSearchOnClick());

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSavedInstanceState");
        //outState.putBoolean(KEY_STATUS, ((MainActivity)getActivity()).getBluetoothSocket().isConnected());

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
        if(mSearchButton.getText().equals("Search"))
            ((SendActivity)getActivity()).pairedDevices();
        else if(mSearchButton.getText().equals("Stop")) {
            mDeviceAdapter.putDevices(mDevices);
        }
        if(mConnected)
            deviceConnected(DataStorage.getInstance().getSocket());

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    private void mSearchOnClick() {
        if(mSearchButton.getText().equals("Search")) {
            mDevices.clear();
            ((SendActivity) getActivity()).searchDevices();
            mSearchProgressBar.setVisibility(View.VISIBLE);
            mSearchButton.setText("Stop");
            mDeviceAdapter.notifyDataSetChanged();
        }
        else
        if (mSearchButton.getText().equals("Stop")){
            ((SendActivity)getActivity()).getBluetoothAdapter().cancelDiscovery();
            mSearchButton.setText("Paired Devices");
        }
        else
        if (mSearchButton.getText().equals("Paired Devices")) {
            ((SendActivity) getActivity()).pairedDevices();
            mSearchButton.setText("Search");
        }
    }

    private void mDisconnectOnClick() {
        try {
            DataStorage.getInstance().getSocket().close();
            //((MainActivity)getActivity()).getBluetoothSocket().
            //setBluetoothSocket(null);
        } catch (IOException e) {
            Log.e(TAG, "Unable to close thread");
        }
        mDisconnectProgressBar.setVisibility(View.VISIBLE);
        //deviceDisconnected();
    }

    public void setDevices(List<BluetoothDevice> devices) {
        Log.d(TAG, "setDevices called");
        //mDevices.clear();
        mDevices = devices;
        mDeviceAdapter = new DeviceAdapter(mDevices, this);
        //mDeviceAdapter.notifyDataSetChanged();
        //Toast.makeText(getActivity(), "devices: " + devices, Toast.LENGTH_SHORT).show();
        mDevicesRecyclerView.setAdapter(mDeviceAdapter = new DeviceAdapter(devices, this));
    }

    public void addDevices(BluetoothDevice device) {
        mDevices.add(device);
        mDeviceAdapter.notifyDataSetChanged();
    }

    /*public void setBluetoothSocket(BluetoothSocket bs){
        ((MainActivity)getActivity()).setBluetoothSocket(bs);
        deviceConnected(bs);
    }*/

    public void deviceConnected(BluetoothSocket bs) {
        mDisconnectButton.setVisibility(View.VISIBLE);
        mDeviceAddressView.setText(bs.getRemoteDevice().getAddress());
        mDeviceNameView.setText(bs.getRemoteDevice().getName());
        mStatus.setText("CONNECTED");
        mStatus.setTextColor(getResources().getColor(R.color.colorConnected));
        mDevicesRecyclerView.setVisibility(View.GONE);
        mSearchButton.setClickable(false);
        mConnected = true;
    }

    public void deviceDisconnected () {
        mDisconnectButton.setVisibility(View.INVISIBLE);
        mDeviceAddressView.setText("null");
        mDeviceNameView.setText("null");
        mStatus.setText("DISCONNECTED");
        mStatus.setTextColor(getResources().getColor(R.color.colorDisconnected));
        mSearchButton.setClickable(true);
        mDevicesRecyclerView.setVisibility(View.VISIBLE);
        mConnected = false;
    }

    public void changeFragment() {
        ((SendActivity)getActivity()).callSendSettingsFragment();
    }

    /*public void setConnectedThread(ConnectedThread connectedThread) {
        ((MainActivity)getActivity()).setConnectedThread(connectedThread);
    }*/

    public void setSearchProgressBarVisibility(boolean b) {
        if(b)
            mSearchProgressBar.setVisibility(View.VISIBLE);
        else
            mSearchProgressBar.setVisibility(View.GONE);
    }

    public void setDisconnectProgressBarVisibility(boolean b) {
        if(b)
            mDisconnectProgressBar.setVisibility(View.VISIBLE);
        else
            mDisconnectProgressBar.setVisibility(View.GONE);
    }

    public String getSearchButtonText() {
        return (String) mSearchButton.getText();
    }

    public void setSearchButtonText(String s) {
        mSearchButton.setText(s);
    }

    public RecyclerView getDevicesRecyclerView() {
        return mDevicesRecyclerView;
    }

    public ProgressBar getSearchProgressBar() {
        return mSearchProgressBar;
    }
}