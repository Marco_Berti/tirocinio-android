package it.univr.esdlab.signalclientnew.activities.send;

import android.support.v4.app.Fragment;
import android.widget.RadioGroup;

public abstract class RadioGroupFragment extends Fragment {
    public abstract void setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener o);
    public abstract String getSelectedRadioButtonText();
}
