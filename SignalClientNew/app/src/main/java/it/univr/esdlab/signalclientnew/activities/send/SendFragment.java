package it.univr.esdlab.signalclientnew.activities.send;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.activities.SendActivity;
import it.univr.esdlab.signalclientnew.singletones.DBSessionToSession;

public class SendFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM0 = "UseMod";
    private static final String ARG_PARAM1 = "pessionID";
    private static final String ARG_PARAM2 = "intervals";
    private static final String ARG_PARAM3 = "intervalDuration";
    private static final String ARG_PARAM4 = "pulse";
    private static final String ARG_PARAM5 = "pulseDuration";
    private static final String ARG_PARAM6 = "vibration";
    private static final String ARG_PARAM7 = "sound";
    private static final String ARG_PARAM8 = "personID";
    private static final String TAG = "SendFragment";

    private int mSessionId;

    private Button mSubButton;
    private TextView mCounter;
    private Button mAdButton;
    private TextView mNotes;
    private Button mStopButton;
    private int mPersonId;
    private boolean mIntervals;//interval checkbox checked
    private int mIntervalDuration;
    private boolean mDuration;
    private int mPulseDuration;
    private boolean mVibration;
    private boolean mSound;
    private String type;
    private int interval;
    private ArrayList<Pair<Long, Long>> freezeDuration;
    private ArrayList<Pair<String, Long>> movements;
    private long lastStartTimestamp = 0;
    private long startRelTimeStamp = 0;
    private Timestamp startAbsTimeStamp;

    private RadioGroupFragment mRadioGroupFragment = null;
    private String mMod;


    public SendFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @param mod
     * @param sessionId ID of actual session.
     * @param intervals is interval checkbox checked.
     * @param intervalsDuration duration of intervals
     * @param duration is pulse checkbox checked
     * @param pulseDuration duration of pulse
     * @param vibration is signal type vibration?
     * @param sound is signal type sound?
     * @return A new instance of fragment SendFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SendFragment newInstance(String mod, int personId, int sessionId, boolean intervals, int intervalsDuration, boolean duration, int pulseDuration, boolean vibration,
                                           boolean sound) {
        SendFragment fragment = new SendFragment();
        Bundle args = new Bundle();

        args.putString(ARG_PARAM0, mod);
        args.putInt(ARG_PARAM1, sessionId);
        args.putBoolean(ARG_PARAM2, intervals);
        args.putInt(ARG_PARAM3, intervalsDuration);
        args.putBoolean(ARG_PARAM4, duration);
        args.putInt(ARG_PARAM5, pulseDuration);
        args.putBoolean(ARG_PARAM6, vibration);
        args.putBoolean(ARG_PARAM7, sound);
        args.putInt(ARG_PARAM8, personId);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMod = getArguments().getString(ARG_PARAM0);
            mPersonId = getArguments().getInt(ARG_PARAM8);
            mSessionId = getArguments().getInt(ARG_PARAM1);
            mIntervals = getArguments().getBoolean(ARG_PARAM2);
            mIntervalDuration = getArguments().getInt(ARG_PARAM3);
            mDuration = getArguments().getBoolean(ARG_PARAM4);
            mPulseDuration = getArguments().getInt(ARG_PARAM5);
            mVibration = getArguments().getBoolean(ARG_PARAM6);
            mSound = getArguments().getBoolean(ARG_PARAM7);
        }
        freezeDuration = new ArrayList<>();
        movements = new ArrayList<>();
        startAbsTimeStamp = new Timestamp(System.currentTimeMillis());//tempo assoluto di inizio della sessoione
        startRelTimeStamp = SystemClock.elapsedRealtime();//tempo relativo alla sessione, calcoli fatti in base a questo tempo
        Log.d(TAG, "Mod = " + mMod);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_send, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSubButton = view.findViewById(R.id.minus_button);
        mSubButton.setOnClickListener(v -> mSubOnClick());

        mCounter = view.findViewById(R.id.counter);

        mAdButton = view.findViewById(R.id.plus_button);
        mAdButton.setOnClickListener(v -> mAddOnClick());

        mStopButton = view.findViewById(R.id.stop_button);
        mStopButton.setOnClickListener(v -> mStopOnClick());

        //Log.d(TAG, "VALORE DI MOD: " + mMod);
        if(mMod.equals("turning"))
            mRadioGroupFragment = new TurningRadioGroupFragment();
        else if(mMod.equals("obstacle"))
            mRadioGroupFragment = new ObstaclesRadioGroupFragment();

        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.radio_group_container, mRadioGroupFragment).commit();
        mRadioGroupFragment.setOnCheckedChangeListener((v, id)-> checkedChange());

        setupButton();
    }

    private void setupButton() {
        if(!mDuration)
            mAdButton.setText("Begin");
        else
            mAdButton.setText("+1");
        //mMovements.check(R.id.sit_radio_button);
    }

    private void checkedChange() {
        String text = mRadioGroupFragment.getSelectedRadioButtonText();
        Log.d(TAG, text);
        movements.add(createMovement(text));
        Log.d(TAG, "Added! size: " + movements.size());
    }

    private Pair<String, Long> createMovement(String state) {
        return new Pair<>(state, SystemClock.elapsedRealtime() - startRelTimeStamp);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void mAddOnClick() {

        long freezeStart;
        long freezeStop;

        if (!mIntervals)
            interval = 0;
        else
            interval = mIntervalDuration;

        if (interval > mPulseDuration)
            Toast.makeText(getContext(), "FAIL: interval must be smaller than duration", Toast.LENGTH_SHORT).show();
        else {

            type = "";

            if(mAdButton.getText().equals("Begin")) {
                mAdButton.setText("End");
                lastStartTimestamp = SystemClock.elapsedRealtime() - startRelTimeStamp;

                if (mVibration) {
                    //send vibration signal
                    ((SendActivity) getActivity()).sendSignalMessage("vibration", mPulseDuration + 1, interval, "manual");
                    type += "manual vibration ";
                }
                if (mSound) {
                    //send sound signal
                    ((SendActivity) getActivity()).sendSignalMessage("sound", mPulseDuration + 1, interval, "manual");
                    type += "manual sound ";
                }

                /*if (mLightCheckBox.isChecked()) {
                    //send light signal
                    ((SendActivity) getActivity()).sendSignalMessage("light", mPulseDuration + 1, interval, "manual");
                    type += "manual light";
                }*/
            }

            else if(mAdButton.getText().equals("+1")) {
                freezeStart = SystemClock.elapsedRealtime() - startRelTimeStamp;
                int counter = Integer.parseInt(mCounter.getText().toString()) + 1;
                mCounter.setText(String.valueOf(counter));

                if (mVibration) {
                    ((SendActivity) getActivity()).sendSignalMessage("vibration", mPulseDuration + 1, interval, "auto");
                    type += "vibration ";
                }
                if (mSound) {
                    ((SendActivity) getActivity()).sendSignalMessage("sound", mPulseDuration + 1, interval, "auto");
                    type += "sound ";
                }
                /*if (mLightCheckBox.isChecked()) {
                    ((SendActivity) getActivity()).sendSignalMessage("light", mPulseDurationSeekBar.getProgress() + 1, interval, "auto");
                    type += "light";
                }*/
                freezeStop = (freezeStart + mPulseDuration) - startRelTimeStamp;
                createFreezeTimestamp(freezeStart,freezeStop);

            }

            else if(mAdButton.getText().equals("End")) {
                ((SendActivity)getActivity()).sendSimpleMessage("END");
                freezeStop = SystemClock.elapsedRealtime() - startRelTimeStamp;
                createFreezeTimestamp(lastStartTimestamp,freezeStop);
                lastStartTimestamp = 0;
                mAdButton.setText("Begin");
                int counter = Integer.parseInt(mCounter.getText().toString()) + 1;
                mCounter.setText(String.valueOf(counter));

            }
        }
    }

    private void mStopOnClick() {
        ((SendActivity)getActivity()).sendSimpleMessage("STOP," + mSessionId);
        Timestamp stopTimeStamp = new Timestamp(System.currentTimeMillis());
        DBSessionToSession.getInstance().createSession(mMod, mSessionId, mPersonId, startAbsTimeStamp, stopTimeStamp, type, mPulseDuration,
                interval, Integer.parseInt(mCounter.getText().toString()), freezeDuration, movements);

        //TODO-> possibiltà di aggiungere il tasto per salvare e cancellare...attualmente chiude diretto(??)
        getActivity().finish();
    }

    private void createFreezeTimestamp(long freezeStart, long freezeStop) {
        Log.d(TAG, freezeStart + " - " + freezeStop);
        Pair<Long, Long> pair = new Pair<>(freezeStart, freezeStop);
        freezeDuration.add(pair);
    }

    private void mSubOnClick() {
        int counter = Integer.parseInt(mCounter.getText().toString()) -1;
        if (counter >= 0)
            mCounter.setText(String.valueOf(counter));
    }
}
