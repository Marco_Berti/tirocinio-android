package it.univr.esdlab.signalclientnew.activities.send;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;

import it.univr.esdlab.signalclientnew.R;
import it.univr.esdlab.signalclientnew.activities.SendActivity;
import it.univr.esdlab.signalclientnew.activities.SettingActivity;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.singletones.DBPersonToPerson;
import it.univr.esdlab.signalclientnew.singletones.DBSessionToSession;

public class SendSettingsFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "personID";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private static final String TAG = "SendSettingsFragment";
    private TextView mNotConnectedTextView;
    private TextView mSelectionTextView;
    private CheckBox mVibrationCheckBox;
    private CheckBox mSoundCheckBox;
    private CheckBox mLightCheckBox;
    private CheckBox mDurationCheckBox;
    private SeekBar mPulseDurationSeekBar;
    private EditText mPulseDurationEditText;
    private CheckBox mIntervalsCheckBox;
    private SeekBar mIntervalDurationSeekBar;
    private EditText mIntervalDurationEditText;
    private Button mStartButton;
    private int sessionId;
    private RadioGroup mModRadioGroup;


    public SendSettingsFragment() {
        // Required empty public constructor
    }

    public static SendSettingsFragment newInstance(Person p) {
        SendSettingsFragment fragment = new SendSettingsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, p.getId());
        //args.putSerializable(ARG_PARAM1, p);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        if (getArguments() != null) {
            //mPerson = (Person) getArguments().getSerializable(ARG_PARAM1);
            int id = getArguments().getInt(ARG_PARAM1);
            //mPerson = DBPersonToPerson.getInstance().getPerson(id);
            //mParam2 = getArguments().getString(ARG_PARAM2);￼￼
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_send_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSelectionTextView = view.findViewById(R.id.signal_type_text);
        mVibrationCheckBox = view.findViewById(R.id.viber_checkbox);
        mSoundCheckBox = view.findViewById(R.id.sound_checkbox);
        mLightCheckBox = view.findViewById(R.id.light_checkbox);

        mDurationCheckBox = view.findViewById(R.id.duration_text);
        mDurationCheckBox.setOnClickListener(v-> mDurationOnClick());

        mPulseDurationSeekBar = view.findViewById(R.id.pulse_duration_seekbar);
        mPulseDurationSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mPulseDurationEditText.setText(String.valueOf(progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mIntervalsCheckBox = view.findViewById(R.id.intervals_checkbox);
        mIntervalsCheckBox.setOnClickListener(v -> mIntervalsOnClick());

        mIntervalDurationSeekBar = view.findViewById(R.id.intervals_duration_seekbar);
        mIntervalDurationSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                /*if (progress == 0)
                    seekBar.setProgress(1);*/
                mIntervalDurationEditText.setText(String.valueOf(progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mPulseDurationEditText = view.findViewById(R.id.pulse_duration_text);
        mPulseDurationEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    //Update Seekbar value after entering a number
                    mPulseDurationSeekBar.setProgress(Integer.parseInt(s.toString()) - 1);
                } catch (Exception ex) {
                    Log.e(TAG, "seekbar error");
                }

            }
        });

        mIntervalDurationEditText = view.findViewById(R.id.intervals_duration_text);
        mIntervalDurationEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    //Update Seekbar value after entering a number
                    mIntervalDurationSeekBar.setProgress(Integer.parseInt(s.toString()) - 1);
                } catch (Exception ex) {
                    Log.e(TAG, "seekbar error");
                }

            }
        });

        mModRadioGroup = view.findViewById(R.id.mod_selection);

        mStartButton = view.findViewById(R.id.sart_button);
        mStartButton.setOnClickListener(v -> mStartOnClick());

    }

    private void mDurationOnClick() {
        if(!mDurationCheckBox.isChecked()) {
            mPulseDurationSeekBar.setEnabled(false);
            mPulseDurationEditText.setEnabled(false);
        }
        else {
            mPulseDurationSeekBar.setEnabled(true);
            mPulseDurationEditText.setEnabled(true);
        }
    }



    private void mIntervalsOnClick () {
        if (mIntervalsCheckBox.isChecked()) {
            mIntervalDurationSeekBar.setEnabled(true);
            mIntervalDurationEditText.setEnabled(true);
        } else {
            mIntervalDurationSeekBar.setEnabled(false);
            mIntervalDurationEditText.setEnabled(false);
        }
    }

    private void mStartOnClick () {
        mStartButton.setEnabled(false);
        mIntervalsCheckBox.setEnabled(false);
        mLightCheckBox.setEnabled(false);
        mSoundCheckBox.setEnabled(false);
        mVibrationCheckBox.setEnabled(false);
        mIntervalDurationSeekBar.setEnabled(false);
        mDurationCheckBox.setEnabled(false);
        mPulseDurationSeekBar.setEnabled(false);
        mIntervalDurationEditText.setEnabled(false);
        mPulseDurationEditText.setEnabled(false);

        String mod;
        if(mModRadioGroup.getCheckedRadioButtonId() == R.id.turning_test)
            mod = "turning";
        else
            mod = "obstacle";

        Log.d(TAG, "VALORE DI MOD: " + mod);

        sessionId = DBSessionToSession.getInstance().getNextId();
        ((SendActivity)getActivity()).sendSimpleMessage("START," + sessionId);
        ((SendActivity)getActivity()).callSendFragment(mod, sessionId, mIntervalsCheckBox.isChecked(), mIntervalDurationSeekBar.getProgress(),
                mDurationCheckBox.isChecked(), (mPulseDurationSeekBar.getProgress()*1000), mVibrationCheckBox.isChecked(), mSoundCheckBox.isChecked());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

}
