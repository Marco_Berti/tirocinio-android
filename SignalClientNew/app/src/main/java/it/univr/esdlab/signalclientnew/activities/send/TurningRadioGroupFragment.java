package it.univr.esdlab.signalclientnew.activities.send;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import it.univr.esdlab.signalclientnew.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TurningRadioGroupFragment extends RadioGroupFragment {

    RadioGroup mRadioGroup = null;
    RadioGroup.OnCheckedChangeListener mOnCheckedChangeListener = null;

    private static final String TAG = "TurningRadioGroupFragme";


    public TurningRadioGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_turning_radio_group, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRadioGroup = view.findViewById(R.id.radio_group);
        if (mOnCheckedChangeListener != null){
            Log.d(TAG, "Setto il listener");
            mRadioGroup.setOnCheckedChangeListener(mOnCheckedChangeListener);
        }
        else
            Log.e(TAG, "listener is null");
        RadioButton rb = view.findViewById(R.id.sit_radio_button);
        rb.setChecked(true);
    }

    @Override
    public void setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener o) {
        mOnCheckedChangeListener = o;
        if (mRadioGroup != null){
            mRadioGroup.setOnCheckedChangeListener(o);
            Log.d(TAG, "Setto subito il listener");
        }
        else
            Log.d(TAG, "Salvo il listener ma non lo setto ancora al radio group");
    }

    @Override
    public String getSelectedRadioButtonText() {
        RadioButton rb = mRadioGroup.findViewById(mRadioGroup.getCheckedRadioButtonId());
        return rb.getText().toString();
    }
}
