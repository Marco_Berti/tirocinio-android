package it.univr.esdlab.signalclientnew.datamapper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;

import com.couchbase.lite.Array;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.DatabaseConfiguration;
import com.couchbase.lite.Dictionary;
import com.couchbase.lite.Document;
import com.couchbase.lite.Endpoint;
import com.couchbase.lite.MutableArray;
import com.couchbase.lite.MutableDictionary;
import com.couchbase.lite.MutableDocument;
import com.couchbase.lite.Replicator;
import com.couchbase.lite.ReplicatorConfiguration;
import com.couchbase.lite.URLEndpoint;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import it.univr.esdlab.signalclientnew.activities.person.PersonFragment;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.model.PersonSmall;
import it.univr.esdlab.signalclientnew.model.Session;
import it.univr.esdlab.signalclientnew.model.SessionSmall;

public class DataMapper {
    private static final DataMapper ourInstance = new DataMapper();
    private static final String TAG = "DataMapper";
    private Database mDatabase;
    private Endpoint targetEndpoint;
    private Replicator replicator;
    private Context mContext;

    public static DataMapper getInstance() {
        return ourInstance;
    }

    private DataMapper() {
    }

    public void startPushReplication() {
        //Database.setLogLevel(LogDomain.REPLICATOR, LogLevel.VERBOSE);
        URI uri = null;
        try {
            uri = new URI(getDestination());
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
        }
        targetEndpoint = new URLEndpoint(uri);
        ReplicatorConfiguration replConfig = new ReplicatorConfiguration(mDatabase,targetEndpoint);
        replConfig.setReplicatorType(ReplicatorConfiguration.ReplicatorType.PUSH);
        ArrayList<String> channels = new ArrayList<>();
        channels.add("client");
        replConfig.setChannels(channels);
        //replConfig.setAuthenticator(new BasicAuthenticator("client", "client_mydb"));
        replicator = new Replicator(replConfig);
        replicator.addChangeListener(change -> {
            if (change.getStatus().getActivityLevel() == Replicator.ActivityLevel.STOPPED)
                com.couchbase.lite.internal.support.Log.e(TAG, "Replication stopped");
        });
        replicator.start();
    }

    public void startPullReplication(PersonFragment owner) {
        //Database.setLogLevel(LogDomain.REPLICATOR, LogLevel.VERBOSE);
        try {
            URI uri = new URI(getDestination());
            targetEndpoint = new URLEndpoint(uri);
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
        }
        ArrayList<String> channels = new ArrayList<>();
        channels.add("client");
        ReplicatorConfiguration replConfig = new ReplicatorConfiguration(mDatabase,targetEndpoint);
        replConfig.setReplicatorType(ReplicatorConfiguration.ReplicatorType.PULL);
        replConfig.setChannels(channels);
        //replConfig.setAuthenticator(new BasicAuthenticator("client", "client_mydb"));
        replicator = new Replicator(replConfig);
        replicator.addChangeListener(change -> {
            if (change.getStatus().getActivityLevel() == Replicator.ActivityLevel.STOPPED) {
                Log.e(TAG, "Replication stopped");
                owner.stopRefreshing();
            }
            if (change.getStatus().getError() != null)
                Log.e(TAG, "ERROR CODE::" + change.getStatus().getError());
        });
        replicator.start();
    }

    public void setContext(Context context) {
        mContext = context;
        Log.e(TAG, "setto " + context + " a " + mDatabase);
        DatabaseConfiguration config = new DatabaseConfiguration(context);
        if (mDatabase == null) {
            try {
                Log.i(TAG, "Creo database");
                mDatabase = new Database("myDB", config);
                Log.i(TAG, "Database creato");
            }
            catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "Destination-> " + getDestination());
        }
    }

    public void addPerson(Person p) {
        MutableArray sessions = new MutableArray();
        for (SessionSmall ss : p.getSessionsList()){
            MutableDictionary mDict = new MutableDictionary()
                    .setInt("id", ss.getId())
                    .setInt("freeze", ss.getFreeze())
                    .setLong("startTimestamp", ss.getStartTimestamp().getTime());
            sessions.addValue(mDict);
        }
        MutableArray channels = new MutableArray();
        channels.addString("client");
        MutableDocument mutableDoc = new MutableDocument(p.getId() + "_person")
                .setString("name", p.getName())
                .setDate("birthday",p.getBirthday().getTime())//passo da calendar a date
                .setString("birthplace",p.getBirthplace())
                .setDate("sicksince", p.getSickSince().getTime())
                .setString("genre",  p.getGenre())
                .setArray("sessionsSmall",sessions)
                .setArray("channels", channels);
        try {
            mDatabase.save(mutableDoc);
            Log.d(TAG, "Persona scritta nel database -> " + mutableDoc.getId() + " che sarebbe " + mutableDoc.getString("name"));
        }
        catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public void addPersonSmall(PersonSmall personSmall) {
        MutableArray channels = new MutableArray();
        channels.addString("client");

        MutableDictionary ps = new MutableDictionary()
                .setString("name", personSmall.getName())
                .setString("age", personSmall.getAge())
                .setInt("id", personSmall.getId());

        MutableArray personSmallArray = new MutableArray();
        Document personSmallDoc = mDatabase.getDocument("personSmallDoc");
        if (personSmallDoc == null) {
            personSmallDoc = new MutableDocument("personSmallDoc");
            personSmallArray.addValue(ps);
        }
        else {
            personSmallArray = personSmallDoc.getArray("values").toMutable();
            personSmallArray.addValue(ps);
        }
        MutableDocument personSmallMutableDoc = personSmallDoc.toMutable();
        personSmallMutableDoc.setArray("values", personSmallArray)
                .setArray("channels", channels);
        try {
            mDatabase.save(personSmallMutableDoc);
        }
        catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        startPushReplication();
    }

    public Person getPerson(int id) {
        Log.i(TAG, "Cerco " + id + " nel database " + mDatabase);

        MutableDocument personDoc = mDatabase.getDocument(id + "_person").toMutable();

        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        cal.setTime(personDoc.getDate("birthday"));
        cal2.setTime(personDoc.getDate("sicksince"));

        Person p = new Person(id, personDoc.getString("name"),
                cal, personDoc.getString("birthplace"),
                cal2, personDoc.getString("genre"));
        Array sessions = personDoc.getArray("sessionsSmall");
        if (sessions != null) {
            for (Object o : sessions) {
                Dictionary d = (Dictionary) o;
                Timestamp timestamp = new Timestamp(d.getLong("startTimestamp"));
                p.addSessionSmall(new SessionSmall(d.getInt("id"), d.getInt("freeze"), timestamp));
                Log.i(TAG, "La persona " + p.getName() + " ha la sessione " + d.getInt("id"));
            }
        } else {
            Log.e(TAG, "La persona " + p.getName() + " non ha sessioni");
        }
        return p;
    }


    public int getLastUsedPersonId() {
        int id;
        Document indexer = mDatabase.getDocument("indexer");

        if(indexer == null) {
            indexer = new MutableDocument("indexer")
                    .setInt("lastPersonId", 0)
                    .setInt("lastSessionId", 0);
            id = indexer.getInt("lastPersonId");
            try {
                mDatabase.save(indexer.toMutable());
            }
            catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }
        }
        else {
            id = indexer.getInt("lastPersonId");
        }
        return id;
    }

    public void incrementLastUsedPersonId() {
        MutableArray channels = new MutableArray();
        channels.addString("client");
        MutableDocument doc = mDatabase.getDocument("indexer").toMutable();

        int id = doc.getInt("lastPersonId");
        id++;
        doc.setValue("lastPersonId", id)
                .setArray("channels", channels);
        try {
            mDatabase.save(doc);
        }
        catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<PersonSmall> getPersonsSmallList() {
        ArrayList<PersonSmall> psList = new ArrayList<>();
        Document personsSmallDoc = mDatabase.getDocument("personSmallDoc");
        if (personsSmallDoc == null)
            return psList;
        Array persons = personsSmallDoc.getArray("values");
        for(Object o : persons) {
            Dictionary d = (Dictionary) o;
            PersonSmall ps = new PersonSmall(d.getInt("id"), d.getString("name"),d.getString("age"));
            psList.add(ps);
        }
        return psList;
    }

    public Session getSession(int sessionId) {
        Document sessionDoc = mDatabase.getDocument(sessionId + "_session");
        Timestamp startTimestamp = new Timestamp(sessionDoc.getLong("startTimestamp"));
        Timestamp stopTimestamp = new Timestamp(sessionDoc.getLong("stopTimestamp"));
        Array freezeDurationArray = sessionDoc.getArray("freezeDuration");
        Array movementsArray = sessionDoc.getArray("movements");

        ArrayList<Pair<Long, Long>> list = new ArrayList<>();
        for (Object o : freezeDurationArray) {
            if (o instanceof Pair) {
                Pair p = (Pair) o;
                list.add(p);
            }
        }

        ArrayList<Pair<String, Long>> movements = new ArrayList<>();
        for (Object o : movementsArray) {
            if(o instanceof  Pair) {
                Pair p = (Pair) o;
                movements.add(p);
            }
        }

        return new Session(sessionDoc.getString("mod"), sessionId, sessionDoc.getInt("person_id"), startTimestamp, stopTimestamp, sessionDoc.getString("type"), sessionDoc.getInt("duration"), sessionDoc.getInt("interval"), sessionDoc.getInt("freeze"), list, movements);
    }

    public void addSession(Session s) {

        MutableDocument doc = new MutableDocument(s.getId() + "_session");
        Array freezeArray = doc.getArray("freezeDuration");
        Array movementsArray = doc.getArray("movements");

        MutableArray channels = new MutableArray();
        channels.addString("client")
                .addString(s.getId() + "_channel");

        doc.setInt("id", s.getId())
                .setString("mod", s.getMod())
                .setInt("person_id", s.getPersonId())
                .setLong("startTimestamp", s.getStartTimeStamp().getTime())
                .setLong("stopTimestamp", s.getStopTimestamp().getTime())
                .setString("type", s.getType())
                .setInt("duration", s.getDuration())
                .setInt("interval", s.getInterval())
                .setInt("freeze", s.getFreeze())
                .setString("notes", s.getNotes())
                .setArray("channels",channels);

        MutableArray freezeMutableArray;
        MutableArray movesMutableArray;

        if(freezeArray == null)
            freezeMutableArray = new MutableArray();
        else
            freezeMutableArray = freezeArray.toMutable();

        for(Pair p : s.getFreezeDuration()) {
            MutableDictionary md = new MutableDictionary()
                    .setLong("start", (Long) p.first)
                    .setLong("stop", (Long) p.second);
            freezeMutableArray.addValue(md);
        }
        doc.setArray("freezeDuration", freezeMutableArray);

        if(movementsArray == null)
            movesMutableArray = new MutableArray();
        else
            movesMutableArray = movementsArray.toMutable();

        for(Pair p : s.getMovements()) {
            MutableDictionary md = new MutableDictionary()
                    .setString("state", (String) p.first)
                    .setLong("timestamp", (Long)p.second);
            movesMutableArray.addValue(md);
        }
        doc.setArray("movements", movesMutableArray);

        try {
            mDatabase.save(doc);
        }
        catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }

        startPushReplication();
    }

    public int getLastUsedSessionId() {
        Document indexer = mDatabase.getDocument("indexer");
        return indexer.getInt("lastSessionId");
    }

    public void incrementLastUsedSessionId() {
        MutableDocument doc = mDatabase.getDocument("indexer").toMutable();

        int id = doc.getInt("lastSessionId");
        id++;
        doc.setValue("lastSessionId", id);
        try {
            mDatabase.save(doc);
        }
        catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public void updatePerson(Person person) {
        addPerson(person);
        Log.d(TAG, "AGGIORNO LA PERSONA");
    }

    private String getDestination() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        String name = pref.getString("db_name", "");
        String url = pref.getString("db_url", "");
        Log.d(TAG, "Destination: " + url+name);
        return url + name;
    }

}
