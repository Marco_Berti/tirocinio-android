package it.univr.esdlab.signalclientnew.model;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Calendar;
import java.util.TreeSet;

import it.univr.esdlab.signalclientnew.datamapper.DataMapper;

public class Person implements Comparable<Person> {

    private OnChangeListener listener;

    private String name;
    private int id;
    private Calendar birthday;
    private String birthplace;
    private Calendar sickSince;
    private String genre;
    private TreeSet<SessionSmall> mSessionsList;

    public Person(int id, String name, Calendar birthday, String birthplace, Calendar sickSince, String genre) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.birthplace = birthplace;
        this.sickSince = sickSince;
        this.genre = genre;
        mSessionsList = new TreeSet<>();
    }

    public String getAge() {
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - birthday.get(Calendar.YEAR);
        if(today.get(Calendar.DAY_OF_YEAR) < birthday.get(Calendar.DAY_OF_YEAR))
            age--;
        Integer intAge = age;
        return intAge.toString();
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public Calendar getSickSince() {
        return sickSince;
    }

    public String getGenre() {
        return genre;
    }

    public TreeSet<SessionSmall> getSessionsList() {
        return mSessionsList;
    }

    @Override
    public String toString() {
        return id + " " + name;
    }

    @Override
    public int compareTo(@NonNull Person o) {
        return this.id - o.id;
    }

    public PersonSmall getSmall() {
        return new PersonSmall(id, name, getAge());
    }

    public void addSessionSmall(SessionSmall sessionSmall) {
        this.mSessionsList.add(sessionSmall);
        Log.i("Person", "Le sessioni di " + name + " sono " + mSessionsList);
    }

    public void setOnChangeListener(OnChangeListener listener) {
        this.listener = listener;
    }

    public interface OnChangeListener {
        void onPersonChanged(Person person);
    }
}

