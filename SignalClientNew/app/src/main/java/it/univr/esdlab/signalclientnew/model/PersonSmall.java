package it.univr.esdlab.signalclientnew.model;

public class PersonSmall implements Comparable<PersonSmall> {

    private int id;
    private String name;
    private String age;

    public PersonSmall(int id, String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    @Override
    public int compareTo(PersonSmall o) {
        return o.id - id;
    }
}
