package it.univr.esdlab.signalclientnew.model;

import android.support.annotation.NonNull;
import android.util.Pair;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Session implements Serializable, Comparable<Session> {
    private String mod;
    private Timestamp startTimestamp;
    private Timestamp stopTimestamp;
    private String type;
    private int duration;
    private int interval;
    private int freeze;
    private String notes;
    private int id;
    private int personId;
    private ArrayList<Pair<Long, Long>> freezeDuration;
    private ArrayList<Pair<String, Long>> movements;

    public Session(String mod, int id, int personId, Timestamp startTimestamp, Timestamp stopTimestamp, String type, int duration, int interval, int freeze, ArrayList<Pair<Long, Long>> freezeDuration, ArrayList<Pair<String, Long>> movements) {

        this.mod = mod;
        this.id = id;
        this.personId = personId;
        this.startTimestamp = startTimestamp;
        this.stopTimestamp = stopTimestamp;
        this.type = type;
        this.duration = duration;
        this.interval = interval;
        this.freeze = freeze;
        this.notes = notes;
        this.freezeDuration = freezeDuration;
        this.movements = movements;
    }

    public int getId() {
        return id;
    }

    public Timestamp getStartTimeStamp() {
        return startTimestamp;
    }

    public Timestamp getStopTimestamp() {
        return stopTimestamp;
    }

    public String getType() {
        return type;
    }

    public int getDuration() {
        return duration;
    }

    public int getInterval() {
        return interval;
    }

    public int getFreeze() {
        return freeze;
    }

    public String getNotes() {
        return notes;
    }

    public SessionSmall getSmall() {
        return new SessionSmall(id, freeze, startTimestamp);
    }

    public ArrayList<Pair<Long, Long>> getFreezeDuration() {
        return freezeDuration;
    }

    public ArrayList<Pair<String, Long>> getMovements() {
        return movements;
    }

    @Override
    public String toString() {
        return type + " " + startTimestamp;
    }

    @Override
    public int compareTo(@NonNull Session s) {
        return (this.startTimestamp.compareTo(s.startTimestamp)*-1);
    }

    public int getPersonId() {
        return personId;
    }

    public String getMod() {
        return mod;
    }
}