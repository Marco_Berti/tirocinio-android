package it.univr.esdlab.signalclientnew.model;

import android.support.annotation.NonNull;

import java.sql.Timestamp;

public class SessionSmall implements Comparable<SessionSmall> {

    private int freeze;
    private Timestamp startTimestamp;
    private int id;

    public SessionSmall(int id, int freeze, Timestamp startTimestamp) {
        this.id = id;
        this.freeze = freeze;
        this.startTimestamp = startTimestamp;
    }

    public int getId() {
        return id;
    }

    public int getFreeze() {
        return freeze;
    }

    public void setFreeze(int freeze) {
        this.freeze = freeze;
    }

    public Timestamp getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Timestamp startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    @Override
    public int compareTo(@NonNull SessionSmall o) {
        return id-o.id;
    }

    public String toString() {
        return "" + id;
    }
}
