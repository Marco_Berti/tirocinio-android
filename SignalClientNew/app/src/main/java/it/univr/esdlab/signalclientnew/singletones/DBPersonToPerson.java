package it.univr.esdlab.signalclientnew.singletones;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import it.univr.esdlab.signalclientnew.datamapper.DataMapper;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.model.PersonSmall;

public class DBPersonToPerson {

    private static final DBPersonToPerson ourInstance = new DBPersonToPerson();
    private Map<Integer, Person> persons;
    private Map<Integer, PersonSmall> personsSmall;

    public static DBPersonToPerson getInstance() {
        return ourInstance;
    }

    private DBPersonToPerson() {
        persons = new HashMap<>();
        personsSmall = new HashMap<>();
    }

    /**
     * Create person from activity saving it in database
     * @param name
     * @param birthday
     * @param birthplace
     * @param sickSince
     * @param genre
     */

    public void createPerson(String name, Calendar birthday, String birthplace, Calendar sickSince, String genre){
        int id = getNextId();
        Person mPerson = new Person(id, name, birthday, birthplace, sickSince, genre);
        PersonSmall mPersonSmall = mPerson.getSmall();
        this.persons.put(id, mPerson);
        this.personsSmall.put(id, mPersonSmall);

        DataMapper.getInstance().addPerson(mPerson);
        DataMapper.getInstance().addPersonSmall(mPersonSmall);
    }

    /**
     * Create person from database without saving it in database again
     * @param name
     * @param birthday
     * @param birthplace
     * @param sickSince
     * @param genre
     */
    public void readPerson(String name, Calendar birthday, String birthplace, Calendar sickSince, String genre) {
        int id = getNextId();
        Person mPerson = new Person(id, name, birthday, birthplace, sickSince, genre);
        PersonSmall mPersonSmall = mPerson.getSmall();
        this.persons.put(id, mPerson);
        this.personsSmall.put(id, mPersonSmall);
    }

    private int getNextId() {
        int lastUsedId = DataMapper.getInstance().getLastUsedPersonId();
        lastUsedId++;
        DataMapper.getInstance().incrementLastUsedPersonId();
        return lastUsedId;
    }

    public Person getPerson(int id){
        if (this.persons.containsKey(id))
            return this.persons.get(id);
        else {
            Person mPerson = DataMapper.getInstance().getPerson(id);
            this.persons.put(id, mPerson);
            return mPerson;
        }
    }

    public ArrayList<PersonSmall> getPersonsSmallList(){
        if (this.personsSmall.isEmpty()){
            ArrayList<PersonSmall> personsArray = DataMapper.getInstance().getPersonsSmallList();
            for (PersonSmall ps: personsArray){
                this.personsSmall.put(ps.getId(), ps);
            }
        }
        ArrayList<PersonSmall> returnList = new ArrayList<>(this.personsSmall.values());
        Collections.sort(returnList);
        return returnList;
    }

}
