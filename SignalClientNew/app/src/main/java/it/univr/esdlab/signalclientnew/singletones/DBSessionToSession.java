package it.univr.esdlab.signalclientnew.singletones;

import android.util.Log;
import android.util.Pair;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import it.univr.esdlab.signalclientnew.datamapper.DataMapper;
import it.univr.esdlab.signalclientnew.model.Person;
import it.univr.esdlab.signalclientnew.model.Session;
import it.univr.esdlab.signalclientnew.model.SessionSmall;

public class DBSessionToSession {

    private static final DBSessionToSession ourInstance = new DBSessionToSession();
    private static final String TAG = "DBSessionToSession";
    private Map<Integer, Session> mSessionMap;
    private Map<Integer, ArrayList<SessionSmall>> mSessionSmallMap;

    public static DBSessionToSession getInstance() {
        return ourInstance;
    }

   private DBSessionToSession() {
        mSessionMap = new HashMap<>();
    }

    public ArrayList<SessionSmall> getPersonSessionsSmall(int personID){
        return new ArrayList<>(DBPersonToPerson.getInstance().getPerson(personID).getSessionsList());
    }

    public Session getSessionDetail(int sessionId){
        if (mSessionMap.containsKey(sessionId))
            return mSessionMap.get(sessionId);
        Session mSession = DataMapper.getInstance().getSession(sessionId);
        mSessionMap.put(sessionId, mSession);
        return mSession;
    }

    public void createSession(String mod, int sessionId, int personId, Timestamp startTimeStamp, Timestamp stopTimestamp, String type, int duration, int interval, int freeze, ArrayList<Pair<Long, Long>> freezeDuration, ArrayList<Pair<String, Long>> movements){
        //int id = getNextId();
        int id = sessionId;
        Session s = new Session(mod, id, personId, startTimeStamp, stopTimestamp, type, duration, interval, freeze, freezeDuration, movements);
        SessionSmall ss = s.getSmall();
        this.mSessionMap.put(s.getId(), s);
        Log.i(TAG, "Ho aggiunto " + s.getId() + "\n E ora la mappa e':\n" + mSessionMap);
        Person person = DBPersonToPerson.getInstance().getPerson(personId);
        person.addSessionSmall(ss);
        DataMapper.getInstance().updatePerson(person);
        DataMapper.getInstance().addSession(s);
    }

    public int getNextId(){
        int lastUsedId = DataMapper.getInstance().getLastUsedSessionId();
        lastUsedId++;
        Log.i(TAG, "Il prossimo id per la sessione e': " + lastUsedId);
        DataMapper.getInstance().incrementLastUsedSessionId();
        return lastUsedId;
    }

}
