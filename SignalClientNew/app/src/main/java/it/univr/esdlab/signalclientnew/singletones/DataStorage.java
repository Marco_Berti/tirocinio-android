package it.univr.esdlab.signalclientnew.singletones;

import android.bluetooth.BluetoothSocket;

import it.univr.esdlab.signalclientnew.threads.SendReceiveThread;

public class DataStorage {

    private static final DataStorage ourInstance = new DataStorage();
    private static final String TAG = "DataStorage";
    private BluetoothSocket mSocket;
    private SendReceiveThread mThread;

    public static DataStorage getInstance() {
        return ourInstance;
    }

    private DataStorage() {
    }

    public BluetoothSocket getSocket() {
        return mSocket;
    }

    public void setSocket(BluetoothSocket socket) {
        mSocket = socket;
    }

    public SendReceiveThread getThread() {
        return mThread;
    }

    public void setThread(SendReceiveThread thread) {
        mThread = thread;
    }
}
