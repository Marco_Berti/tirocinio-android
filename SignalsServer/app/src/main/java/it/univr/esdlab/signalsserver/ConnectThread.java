package it.univr.esdlab.signalsserver;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

/**
 * Therad di connessione verso server su dispositivo wear.
 */
public class ConnectThread extends Thread {
    private static final String TAG = "Connection thread";
    private static final UUID MY_UUID = UUID.fromString("b755e5a4-3bd7-4284-b4b9-979e9aba851b");
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private final DeviceHolder owner;

    public ConnectThread(DeviceHolder deviceHolder) {
        // Use a temporary object that is later assigned to mmSocket
        // because mmSocket is final.
        owner = deviceHolder;
        BluetoothDevice device = deviceHolder.getBluetoothDevice();
        BluetoothSocket tmp = null;
        mmDevice = device;

        try {
            // Get a BluetoothSocket to connect with the given BluetoothDevice.
            // MY_UUID is the app's UUID string, also used in the server code.
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            Log.e(TAG, "Socket's create() method failed", e);
        }
        mmSocket = tmp;
    }

    public void run() {
        // Cancel discovery because it otherwise slows down the connection.
        mBluetoothAdapter.cancelDiscovery();
        Log.d(TAG, "Thread started");
        System.out.println("Thread started");
    /*Handler myHandler new Handler(Looper.getMainLooper());
    myHandler.post(() -> owner.*/

        try {
            // Connect to the remote device through the socket. This call blocks
            // until it succeeds or throws an exception.
            mmSocket.connect();
        } catch (IOException connectException) {
            // Unable to connect; close the socket and return.
            try {
                mmSocket.close();
                DataStorage.getInstance().setWearSocket(null);
                Handler myHandler = new Handler(Looper.getMainLooper());
                myHandler.post(() -> owner.setTitleTextViewEnabled(true));
            } catch (IOException closeException) {
                Log.e(TAG, "Could not close the client socket", closeException);
            }
            return;
        }

        // The connection attempt succeeded. Perform work associated with the connection in a separate thread.
        manageMyConnectedSocket(mmSocket);
    }

    // Closes the client socket and causes the thread to finish.

    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Could not close the client socket", e);
        }
    }

    /**
     * gestione della socket creata con modifica dei fragment mostrati.
     * @param mmSocket socket aparta durante la connessione.
     */
    private void manageMyConnectedSocket(BluetoothSocket mmSocket) {
        DataStorage.getInstance().setWearSocket(mmSocket);
        SendReceiveThread srt = new SendReceiveThread(mmSocket);

        DataStorage.getInstance().setWearThread(srt);

        Handler myHandler = new Handler(Looper.getMainLooper());
        myHandler.post(() -> {
            //owner.setBluetoothSocket(mmSocket);
            owner.setTitleTextViewEnabled(true);
            //TODO-> sistemare qui, adattarlo per chiudre fragment in fragment
            owner.changeFragment(true);
        });
        Log.d(TAG, "CONNECTION STABILITA - socket: " + mmSocket);
        Log.d(TAG, "socket salvata: " + DataStorage.getInstance().getWearSocket());


    }
}