package it.univr.esdlab.signalsserver;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * Fragment di connessione verso il dispositivo wear
 */
public class ConnectionButtonFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private boolean mConnected;
    private Button mConnectButton;


    public ConnectionButtonFragment() {
        // Required empty public constructor
    }

    public static ConnectionButtonFragment newInstance(boolean connected) {
        ConnectionButtonFragment fragment = new ConnectionButtonFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, connected);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            mConnected = getArguments().getBoolean(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_connection_button, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mConnectButton = view.findViewById(R.id.connect_wear_button);
        mConnectButton.setOnClickListener(v -> onButtonClick());
        if(mConnected)
            mConnectButton.setText("Disconnect wear");
        else
            mConnectButton.setText("Connect wear");

    }

    /**
     * gestione della pressione del bottone di connessione/disconeesione.
     */
    private void onButtonClick() {
        if(mConnected)
            ((MainActivity)getActivity()).changeWearConnectionFragment(false);
        else
            ((MainActivity)getActivity()).startWearConnectionFragment();
    }
}
