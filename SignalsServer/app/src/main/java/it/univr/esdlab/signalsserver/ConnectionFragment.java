package it.univr.esdlab.signalsserver;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.ArrayList;

/**
 * Fragent di connessione verso dispositivo wear
 */
public class ConnectionFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "ConnectionFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView mRecyclerView;
    private DeviceAdapter mDeviceAdapter;
    private ArrayList<BluetoothDevice> mDevices = new ArrayList<>();
    private Button mSearchButton;
    private ProgressBar mSearchProgressBar;


    public ConnectionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConnectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConnectionFragment newInstance(String param1, String param2) {
        ConnectionFragment fragment = new ConnectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDeviceAdapter = new DeviceAdapter(new ArrayList<>(), this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_connection, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = view.findViewById(R.id.recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mDeviceAdapter);
        mSearchButton = view.findViewById(R.id.wear_search_button);
        mSearchButton.setOnClickListener(v -> mSearchOnClick());
        mSearchProgressBar = view.findViewById(R.id.search_progress_bar);
        mSearchProgressBar.setVisibility(View.INVISIBLE);
        mSearchProgressBar.setIndeterminate(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
        if(mSearchButton.getText().equals("Search"))
            ((MainActivity)getActivity()).pairedDevices();
        else if(mSearchButton.getText().equals("Stop")) {
            mDeviceAdapter.putDevices(mDevices);
        }
        /*if(mConnected)
            deviceConnected(DataStorage.getInstance().getSocket());*/
    }

    private void mSearchOnClick() {
        Log.d(TAG, "premuto il tasto di search");
        if(mSearchButton.getText().equals("Search")) {
            mDevices.clear();
            ((MainActivity) getActivity()).searchDevices();
            mSearchProgressBar.setVisibility(View.VISIBLE);
            mSearchButton.setText("Stop");
            mDeviceAdapter.notifyDataSetChanged();
        }
        else
        if (mSearchButton.getText().equals("Stop")){
            ((MainActivity)getActivity()).getBluetoothAdapter().cancelDiscovery();
            mSearchButton.setText("Paired Devices");
        }
        else
        if (mSearchButton.getText().equals("Paired Devices")) {
            mDevices.clear();
            mDeviceAdapter.notifyDataSetChanged();
            ((MainActivity) getActivity()).pairedDevices();
            mSearchButton.setText("Search");
        }
    }

    public void addDevice(BluetoothDevice device) {
        mDevices.add(device);
        mDeviceAdapter.putDevices(mDevices);
    }

    public void setSearchProgressBarVisibility(boolean b) {
        if(b)
            mSearchProgressBar.setVisibility(View.VISIBLE);
        else
            mSearchProgressBar.setVisibility(View.GONE);
    }

    public String getSearchButtonText() {
        return (String) mSearchButton.getText();
    }

    public void setSearchButtonText(String s) {
        mSearchButton.setText(s);
    }

    /**
     * cambio fragment a seconda dello stato.
     * @param connected indica lo stato di connessione del dispositivo wear.
     */
    public void changeFragment(boolean connected) {
        ((MainActivity)getActivity()).changeWearConnectionFragment(connected);
    }
}
