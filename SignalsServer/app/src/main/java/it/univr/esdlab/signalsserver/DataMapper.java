package it.univr.esdlab.signalsserver;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.couchbase.lite.Array;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.DatabaseConfiguration;
import com.couchbase.lite.Document;
import com.couchbase.lite.Endpoint;
import com.couchbase.lite.MutableArray;
import com.couchbase.lite.MutableDictionary;
import com.couchbase.lite.MutableDocument;
import com.couchbase.lite.Replicator;
import com.couchbase.lite.ReplicatorConfiguration;
import com.couchbase.lite.URLEndpoint;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Singletone di interfaccia verso database.
 */
class DataMapper {
    private static final DataMapper ourInstance = new DataMapper();
    private static final String TAG = "DataMapper";
    private Database mDatabase;
    private Document doc;
    private Context mContext;

    static DataMapper getInstance() {
        return ourInstance;
    }

    private DataMapper() {
    }

    /**
     * Uso il context dell'applicazione per la creazione del database
     * @param context context della main activity
     */
    public void setContext(Context context) {
        Log.e(TAG, "setto " + context + " a " + mDatabase);
        DatabaseConfiguration config = new DatabaseConfiguration(context);
        mContext = context;
        if (mDatabase == null) {
            try {
                Log.i(TAG, "Creo database");
                mDatabase = new Database("myDB", config);
                Log.i(TAG, "Database creato");
            }
            catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Salvataggio dei dati della sessione all'interno del database locale
     * @param sessionId id della sessione
     * @param periodSample classe che contiente i campioni salvati fino al momento corrente
     */
    public void addDataSubSection(int sessionId, PeriodSample periodSample) {
        MutableDocument newDoc = new MutableDocument(periodSample.getId());

        Array accelArray = makeArrayFromSensorData(periodSample.getAccelData());
        Array linAccArray = makeArrayFromSensorData(periodSample.getLinearAccelData());
        Array gyroArray = makeArrayFromSensorData(periodSample.getGyroData());
        Array magnArray = makeArrayFromSensorData(periodSample.getMagnetoData());

        newDoc.setArray("accel", accelArray)
                .setArray("linAcc", linAccArray)
                .setArray("gyro",gyroArray)
                .setArray("magn",magnArray);

        String channel = sessionId+"_channel";
        newDoc.setArray("channels", new MutableArray().addString(channel));

        try {
            mDatabase.save(newDoc);
        }
        catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }


    }

    /**
     * Creo l'array da salvare nel database partendo dai dati grezzi di ogni sensore.
     * @param sensorData lista dei dati grezzi provenienti da un sensore.
     * @return array pronto alla scrittura su database.
     */
    private MutableArray makeArrayFromSensorData(List<SensorData> sensorData) {
        MutableArray toReturn = new MutableArray();
        for(SensorData sd : sensorData) {
            MutableDictionary dictionary = new MutableDictionary()
                    .setDouble("X", sd.getX())
                    .setDouble("Y", sd.getY())
                    .setDouble("Z", sd.getZ())
                    .setLong("timestamp", sd.getTimeStamp());
            toReturn.addValue(dictionary);
        }
        return toReturn;
    }

    /**
     * Creazione del replicator con la sua destinazione remota e modalità push per il salvataggio dei
     * dati sul database remoto. Utilizzo dei channels per separare i vari documenti al fine di
     * rendere più efficace l'upload ed evitare la copia di documenti inutili.
     * @param sessionId id della sessione da salvare
     */
    public void startPushReplication(int sessionId) {
        //Database.setLogLevel(LogDomain.REPLICATOR, LogLevel.VERBOSE);
        //String destination = PreferencesMapper.getInstance().getDestination();

        URI uri = null;
        try {
            uri = new URI(getDestination());
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Endpoint targetEndpoint = new URLEndpoint(uri);
        ReplicatorConfiguration replConfig = new ReplicatorConfiguration(mDatabase,targetEndpoint);
        replConfig.setReplicatorType(ReplicatorConfiguration.ReplicatorType.PUSH);
        //replConfig.setAuthenticator(new BasicAuthenticator("client", "client_mydb"));
        ArrayList<String> channels = new ArrayList<>();
        channels.add(sessionId + "_channel");
        Replicator replicator = new Replicator(replConfig);
        replicator.addChangeListener(change -> {
            if (change.getStatus().getActivityLevel() == Replicator.ActivityLevel.STOPPED) {
                com.couchbase.lite.internal.support.Log.e(TAG, "Replication stopped");
                Toast.makeText(mContext, "Replication complete", Toast.LENGTH_SHORT).show();
                /*try {
                    mDatabase.purge(doc);
                } catch (CouchbaseLiteException e) {
                    e.printStackTrace();
                }*/
            }
            if (change.getStatus().getError() != null)
                Log.e(TAG, "ERROR CODE::" + change.getStatus().getError());
        });
        replicator.start();
        Log.d(TAG, "rerplicator started");
    }

    /**
     * estrazione della destinazione di replicazione dai settings dell'app.
     * @return url e nome del database sul quale fare la replicacosa
     */
    private String getDestination() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        String name = pref.getString("db_name", "");
        String url = pref.getString("db_url", "");
        Log.d(TAG, "Destination: " + url+name);
        return url + name;
    }

    /**
     * Creo il documento contenente i dati o le informazioni sui sottodati relativi ad una data sessione
     * @param startAbsTime tempo assoluto di start della sessione.
     * @param stopAbsTime tempo assoluto di stop della sessione.
     * @param subsections array che indica in quante subsection sono separati i dati
     * @param sessionId id della sessione alla quale si fa riferimento.
     */
    public void addDataDocument(long startAbsTime, long stopAbsTime, LinkedList<String> subsections, int sessionId) {
        MutableDocument newDoc = new MutableDocument(sessionId+"_data");
        MutableArray subsectionArray = new MutableArray();
        for (String s : subsections)
            subsectionArray.addString(s);
        newDoc.setLong("start", startAbsTime)
                .setLong("stop", stopAbsTime)
                .setArray("subsections", subsectionArray);

        String channel = sessionId+"_channel";
        newDoc.setArray("channels", new MutableArray().addString(channel));

        try {
            mDatabase.save(newDoc);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }
}
