package it.univr.esdlab.signalsserver;

import android.bluetooth.BluetoothSocket;

/**
 * Classe singletone di salvataggio informazioni interne utili.
 */
public class DataStorage {

    private static final DataStorage ourInstance = new DataStorage();
    private static final String TAG = "DataStorage";
    private BluetoothSocket mSocket;
    private SendReceiveThread mThread;
    private BluetoothSocket mWearSocket;
    private SendReceiveThread mWearThread;

    public static DataStorage getInstance() {
        return ourInstance;
    }

    private DataStorage() {
    }

    public BluetoothSocket getWearSocket() {
        return mWearSocket;
    }

    public void setWearSocket(BluetoothSocket wearSocket) {
        mWearSocket = wearSocket;
    }

    public SendReceiveThread getWearThread() {
        return mWearThread;
    }

    public void setWearThread(SendReceiveThread wearThread) {
        mWearThread = wearThread;
    }

    public BluetoothSocket getSocket() {
        return mSocket;
    }

    public void setSocket(BluetoothSocket socket) {
        mSocket = socket;
    }

    public SendReceiveThread getThread() {
        return mThread;
    }

    public void setThread(SendReceiveThread thread) {
        mThread = thread;
    }
}