package it.univr.esdlab.signalsserver;

import java.util.LinkedList;

/**
 * Therad di upload di una singola subsection senza stoppare la raccolta dati.
 */
class DataUploaderThread extends Thread {

    private final int mSectionId;
    private long mStartAbsTime;
    private long mStopAbsTime;
    private LinkedList<String> subsections;

    public DataUploaderThread(long startAbsTime, long stopAbsTime, LinkedList<String> subsections, int sectionId) {
        this.mStartAbsTime = startAbsTime;
        this.mStopAbsTime = stopAbsTime;
        this.subsections = subsections;
        this.mSectionId = sectionId;
    }

    @Override
    public void run() {
        DataMapper.getInstance().addDataDocument(mStartAbsTime, mStopAbsTime, subsections, mSectionId);
    }
}
