package it.univr.esdlab.signalsserver;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Adapter per la gestione della recycler view dei dispositivi wear disponibili
 */
public class DeviceAdapter extends RecyclerView.Adapter<DeviceHolder> {
    private List<BluetoothDevice> mDevices;
    private ConnectionFragment mCf;

    public DeviceAdapter(List<BluetoothDevice> devices, ConnectionFragment cf) {
        mDevices = devices;
        mCf = cf;
        notifyDataSetChanged();
    }

    @NonNull
    public DeviceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mCf.getActivity());
        View view = layoutInflater
                .inflate(R.layout.device_info, parent, false);
        return new DeviceHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceHolder holder, int position) {
        BluetoothDevice device = mDevices.get(position);
        holder.setBluetoothDevice(device);
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    /*public void setBluetoothSocket(BluetoothSocket bs) {
        mCf.setBluetoothSocket(bs);
    }*/

    public void putDevices(List<BluetoothDevice> devices) {
        mDevices = devices;
        notifyDataSetChanged();
    }

    public void changeFragment(boolean connected) {
        mCf.changeFragment(connected);
    }

    /*public void setConnectedThread(ConnectedThread connectedThread) {
        mCf.setConnectedThread(connectedThread);
    }*/
}
