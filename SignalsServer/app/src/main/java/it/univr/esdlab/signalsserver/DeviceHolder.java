package it.univr.esdlab.signalsserver;

import android.bluetooth.BluetoothDevice;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class DeviceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private static final String TAG = "DeviceHolder";
    private DeviceAdapter owner;
    private BluetoothDevice mBluetoothDevice;
    private TextView mDeviceName;
    private TextView mDeviceAddress;

    public TextView getDeviceName() {
        return mDeviceName;
    }

    public void setTitleTextViewEnabled(boolean b ) {
        itemView.setEnabled(b);
        if(b)
            itemView.setAlpha(1f); //brutto forte ma funziona
        else
            itemView.setAlpha(.5f);
    }

    public DeviceHolder(View itemView, DeviceAdapter owner) {
        super(itemView);
        this.owner = owner;
        mDeviceName = itemView.findViewById(R.id.device_name);
        mDeviceAddress = itemView.findViewById(R.id.device_address);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        ConnectThread ct = new ConnectThread(this);
        //owner.showProgressDialog(ct);
        ct.start();
        itemView.setEnabled(false);
        itemView.setAlpha(.5f); // è brutto forte ma funziona;
        //owner.getDevicesRecyclerView().setClickable(false);

    }

    public void setBluetoothDevice(BluetoothDevice mBluetoothDevice) {
        this.mBluetoothDevice = mBluetoothDevice;
        mDeviceName.setText(mBluetoothDevice.getName());
        mDeviceAddress.setText(mBluetoothDevice.getAddress());
    }

    public BluetoothDevice getBluetoothDevice() {
        return mBluetoothDevice;
    }

    public void changeFragment(boolean connected) {
        owner.changeFragment(connected);
    }

    /*public void setBluetoothSocket(BluetoothSocket bs) {
        owner.setBluetoothSocket(bs);
    }*/


    /*public void setConnectedThread(ConnectedThread connectedThread) {
        owner.setConnectedThread(connectedThread);
    }*/
}
