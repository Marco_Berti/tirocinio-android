package it.univr.esdlab.signalsserver;

import java.util.ArrayList;
import java.util.Date;

import it.univr.esdlab.signalsserver.SensorData;

/**
 * Created by stefano on 10/09/2017.
 */

public class ExerciseData {
    public String Username;
    public String ExerciseTime;
    public long ExerciseLength;
    public String DataClass;
    public ArrayList<SensorData> GyroscopeData;
    public ArrayList<SensorData> AccelerometerData;
    public ArrayList<SensorData> LinearAccelerometerData;
    public ArrayList<SensorData> magnetoData;

    public ExerciseData(String username, String exerciseTime, long  exerciseLength, String dataClass, ArrayList<SensorData> gyroscopeData, ArrayList<SensorData> accelerometerData, ArrayList<SensorData> linearAccelerometerData, ArrayList<SensorData> magnetoData)
    {
        this.Username = username;
        this.ExerciseTime = exerciseTime;
        this.ExerciseLength = exerciseLength;
        this.DataClass = dataClass;
        this.GyroscopeData = gyroscopeData;
        this.AccelerometerData = accelerometerData;
        this.LinearAccelerometerData = linearAccelerometerData;
        this.magnetoData = magnetoData;
    }

    @Override
    public String toString() {
        return "\nuser: " + Username + " time: " + ExerciseTime + "\ngyroscope: " + GyroscopeData.toString() + "\naccelerometer: " + AccelerometerData.toString() + "\nlinear accelerometer: " + LinearAccelerometerData.toString() + "\nmagnet: " + magnetoData.toString();
    }
}
