package it.univr.esdlab.signalsserver;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_DISCOVERY = 2;
    private static final int ACCESS_COARSE_LOCATION = 99;
    private static final String TAG = "MainActivity";
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private AcceptThread mAcceptThread;
    private BluetoothDevice connectedDevice;
    private BluetoothDevice mConnectedWearDevice;
    private ConnectedThread mConnectedThread;
    private MainFragment mMainFragment = null;
    private ConnectionFragment mConnectionFragment = null;
    private Vibrator mVibrator;
    private SensorDataManager mSensorDataManager;
    private Handler mHandler;
    private MediaPlayer mp;
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;

    public void setConnectedThread(ConnectedThread connectedThread) {
        mConnectedThread = connectedThread;
    }

    public BluetoothAdapter getBluetoothAdapter() {
        return mBluetoothAdapter;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                Log.d(TAG, "Connessione stabilita");
                if(DataStorage.getInstance().getSocket() != null)
                    mMainFragment.setConnectedDevice(DataStorage.getInstance().getSocket().getRemoteDevice());
                else if(DataStorage.getInstance().getWearSocket() != null)
                    mMainFragment.setConnectedWearDevice(DataStorage.getInstance().getWearSocket().getRemoteDevice());
            } else if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)){
                Log.d(TAG, "Connessione chiusa");
                //TODO-> mettere a null le socket quando si chiudono
                mMainFragment.disconnectedDevice();
                if(mAcceptThread != null && mAcceptThread.getState().equals(Thread.State.TERMINATED)) {
                    Log.d(TAG,mAcceptThread.getState().toString());
                    startThread();
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                Toast.makeText(getApplicationContext(), "Discovery started...", Toast.LENGTH_SHORT).show();
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                mConnectionFragment.addDevice(device);
                Log.i(TAG, "Device found: " + device.getName() + "; MAC " + device.getAddress());
            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Toast.makeText(getApplicationContext(), "Discovery finished", Toast.LENGTH_SHORT).show();
                mConnectionFragment.setSearchProgressBarVisibility(false);
                if(mConnectionFragment.getSearchButtonText().equals("Stop"))
                    mConnectionFragment.setSearchButtonText("Paired Devices");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if(mMainFragment == null)
            mMainFragment = new MainFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame_layout, mMainFragment).commit();
//TODO-> ipotetici controlli che sia null per non sovraistnziare cose. Forse è una cazzata perché non dovrebbe succedere
        mVibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        mp = MediaPlayer.create(this, R.raw.beep);
        mSensorDataManager = new SensorDataManager();
        DataMapper.getInstance().setContext(getApplicationContext());
        mSensorDataManager.init(getApplicationContext());
        mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SignalsServer::SensorsWakeLock");

    }

    /**
     * Inizializzazione. Chiamata da on start se non connesso crea il filtro per le azioni che possono
     * essere riconosciute. A seguito attiva bluetooth e la sua visibilità facendo poi partire la thread
     * di accetazine della connessione e il fragment di connessione verso il dispositivo wear
     */
    private void initialize() {

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        registerReceiver(mReceiver, filter);

        if (mBluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            Toast.makeText(this, "Bluetooth is not supported", Toast.LENGTH_SHORT).show();
        }
        else
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        else
        if(mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            //rende il dispositivo visibile per 300 secondi
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivityForResult(discoverableIntent, REQUEST_DISCOVERY);
        }
        else {
            mMainFragment.setVisibility("VISIBLE");
            if (DataStorage.getInstance().getSocket() != null) {
                connectedDevice = DataStorage.getInstance().getSocket().getRemoteDevice();
                mMainFragment.setConnectedDevice(connectedDevice);
            } else {
                startThread();
            }
        }


        if(DataStorage.getInstance().getWearSocket() != null) {
            mConnectedWearDevice = DataStorage.getInstance().getWearSocket().getRemoteDevice();
            if (mConnectedWearDevice.getAddress() == null)
                changeWearConnectionFragment(false);
            else
                changeWearConnectionFragment(true);
        }
        else
            changeWearConnectionFragment(false);




    }

    /**
     * Gestisce le risposte dell'utente ai popup generati dalla richiesta di visibilità bluetooth
     * @param requestCode
     * @param resultCode
     * @param data
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);

        if(requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
            //rende il dispositivo visibile per 300 secondi
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivityForResult(discoverableIntent, REQUEST_DISCOVERY);
        }
        if (requestCode == REQUEST_DISCOVERY && resultCode == 300) {

            mMainFragment.setVisibility("VISIBLE");

            if (DataStorage.getInstance().getSocket() != null) {
                Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
                connectedDevice = DataStorage.getInstance().getSocket().getRemoteDevice();
                mMainFragment.setConnectedDevice(connectedDevice);
            } else {
                Toast.makeText(this, "Bluetooth enabled, Start thread", Toast.LENGTH_SHORT).show();
                startThread();
            }
        }
        else
            mMainFragment.setVisibility("NOT VISIBLE");
    }

    /**
     * Crea e lancia thread di accetazione di una connessione bluetooth.
     */
    void startThread() {
        mAcceptThread = new AcceptThread(this);
        mAcceptThread.start();
    }

    /**
     * Verifica i permessi di coarse location necessari per la ricerca bluetooth e li richiede se
     * non ancora concessi. Successivamente inizia effettiva ricerca dispositivi.
     * Connessione verso dispositivo wear.
     */
    public void searchDevices() {
        if (!checkLocationPermissions())
            requestLocationPermissions();
        mBluetoothAdapter.startDiscovery();
    }

    /**
     * Controllo accesso alla posizione per ricerca bluetooth.
     * @return true se l'accesso alla posizione è consentito, false se non consentito.
     */
    public boolean checkLocationPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        else {
            // Permission has already been granted
            return true;
        }
    }

    /**
     * Richiede accesso alla posizione(coarse location).
     */
    public void requestLocationPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ACCESS_COARSE_LOCATION);

        // ACCESS_COARSE_LOCATION is an
        // app-defined int constant. The callback method gets the
        // result of the request.

    }

    /**
     * Gestione risposta al dialog di richesta accesso ai permessi, nel caso specifico alla posizione.
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ACCESS_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    //Toast.makeText(this, "LOCATION GRANTED", Toast.LENGTH_SHORT).show();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "You have to allow access to position!", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (mConnectedThread == null)
            initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        mSensorDataManager.onResume();
        if (DataStorage.getInstance().getSocket() != null && DataStorage.getInstance().getSocket().isConnected())
            mMainFragment.setConnectedDevice(DataStorage.getInstance().getSocket().getRemoteDevice());
        else
            mMainFragment.disconnectedDevice();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * avvio dell'activity di settings dall'overflow menu in alto a dx(tre puntini).
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Lettura del messaggio ricevuto sulla thread, avvio registraioni sensori e creazione dei
     * pattern di vibrazione. Formattazione del messaggio:
     * -START/STOP,vibrazione/suono/luce,durata totale,durata intervalli,manuale/automatico
     * -END applicato solo nlla modalità manuale, indica l'uscita dallo stato di freeze.
     * Vengono fatti i calcoli per il pattern di vibrazione.
     * @param message stringa che indica il messaggio ricevuto
     */
    public void readMessage(String message) {
        sendMessage("stop");
        Log.d(TAG, "MESSAGGIO INVIATO: " + message);

        if(message.contains("START")) {
            String[] s;
            s = message.split(",");
            startSensorsRecording(Integer.parseInt(s[1]));
        }
        else if(message.contains("STOP")){
            String[]s;
            s = message.split(",");
            stopSensorsRecording(Integer.parseInt(s[1]));
        }
        else if(message.equals("END")) {
            if(DataStorage.getInstance().getWearSocket() != null && DataStorage.getInstance().getWearSocket().isConnected())
                DataStorage.getInstance().getWearThread().write(message.getBytes());
            mVibrator.cancel();
            //TODO->aggiungere stop anche del suono
        }

        else {
            if(DataStorage.getInstance().getWearSocket() != null && DataStorage.getInstance().getWearSocket().isConnected())
                DataStorage.getInstance().getWearThread().write(message.getBytes());

            String[] s = message.split(",");
            int duration = Integer.parseInt(s[1]);
            int interval = Integer.parseInt(s[2]);
            duration = duration * 1000;
            boolean manual = false;
            if (s[3].equals("manual"))
                manual = true;

            if (s[0].equals("vibration")) {
                if (interval != 0) {
                    int pulseNumber = duration / interval;
                    long[] pattern = new long[pulseNumber * 2];
                    for (int i = 0; i < pulseNumber * 2; i += 2) {
                        pattern[i] = interval / 2;
                        pattern[i + 1] = interval / 2;
                    }
                    if (!manual) {
                        Log.d(TAG, "pattern" + Arrays.toString(pattern));
                        //discontinuousVibration(pattern, -1);
                        VibrationThread vt = new VibrationThread(mVibrator, pattern, -1);
                        vt.start();
                        //TODO manda messaggio con patern e repeat

                    }
                    else {
                        //discontinuousVibration(pattern, 0);
                        long[] freePattern = {300,0};
                        Log.d(TAG, "pattern" + Arrays.toString(freePattern));
                        VibrationThread vt = new VibrationThread(mVibrator, freePattern, 0);
                        vt.start();
                    }
                }
                else {
                    long[] pattern = {300, 0};
                    Log.d(TAG, "pattern" + Arrays.toString(pattern));
                    //discontinuousVibration(pattern, 0);
                    VibrationThread vt = new VibrationThread(mVibrator, pattern, 0);
                    vt.start();
                }
            }
            if(s[0].equals("sound")) {
                //mp = MediaPlayer.create(this, R.raw.beep);
                SoundThread st = new SoundThread(mp, duration, interval);
                st.start();
            }
        }
        /*if(s[0].equals("light")) {
            continuousLed(duration);
        }*/
    }

    /**
     * acquisice wakelock per la registrazione senza errori a schermo blocato, pulisce le strutture
     * dati, avvia la raccolta dati con e mostra un tost
     * @param id id della sessione alla quale il record fa riferimento
     */
    private void startSensorsRecording(int id) {
        Log.d(TAG, "startRecording");
        mWakeLock.acquire(60*60*1000);
        mSensorDataManager.clearData();
        mSensorDataManager.setStartRecording(id, true);
        fammiUnToast("Recording started");
    }

    /**
     * rilascia wakelock, stoppa la raccolta di dati.
     * @param id id della sessione alla quale il record fa riferimento
     */
    private void stopSensorsRecording(int id) {
        fammiUnToast("Recording stopped - waiting for sync...");
        mWakeLock.release();
        mSensorDataManager.setStartRecording(id,false);
        //DataMapper.getInstance().startPushReplication();

        //ExerciseData test = mSensorDataManager.getCurrentData("test", true);
        //Log.e(TAG, test.toString());
    }

    public void sendMessage(String message) {
        mConnectedThread.write(message.getBytes());
    }

    public void fammiUnToast(String text) {
        mHandler = new Handler(Looper.getMainLooper());
        mHandler.post(() -> Toast.makeText(this, text, Toast.LENGTH_SHORT).show());
    }

    public void startWearConnectionFragment() {
        mConnectionFragment = new ConnectionFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.bluetooth_devices_frame, mConnectionFragment).commit();
    }

    /**
     * mostra, se esistendi, i paired devices per la connessione verso wear
     */
    public void pairedDevices() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        Log.d(TAG, "paired devices: " + pairedDevices);
        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for(BluetoothDevice bd : pairedDevices) {
                mConnectionFragment.addDevice(bd);
            }
        }
        else
            Toast.makeText(this, "No paired devices, please search", Toast.LENGTH_SHORT).show();
    }

    /**
     * mostra o nasconde il fragment di connessione verso wear.
     * @param connected stato connessione dispositivo wear
     */
    public void changeWearConnectionFragment(boolean connected) {
        if(!connected)
            disconnectWear();
        ConnectionButtonFragment connectionButtonFragment = ConnectionButtonFragment.newInstance(connected);
        getSupportFragmentManager().beginTransaction().replace(R.id.bluetooth_devices_frame, connectionButtonFragment).commit();
    }

    /**
     * verifica la connessione verso il dispositivo wear e se disconnesso chiude la scoket così da
     * prepararla per la prossima connessione.
     */
    private void disconnectWear() {
        //TODO-> verrificare se stai facendo giusto
        if(DataStorage.getInstance().getWearSocket() != null && DataStorage.getInstance().getWearSocket().isConnected()) {
            try {
                DataStorage.getInstance().getWearSocket().close();
                Log.d(TAG, "HO CHIUSO LA SOCKET");
                Log.d(TAG, "" + DataStorage.getInstance().getWearSocket().isConnected());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
