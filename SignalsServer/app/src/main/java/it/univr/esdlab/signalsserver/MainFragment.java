package it.univr.esdlab.signalsserver;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.LinkedList;


public class MainFragment extends Fragment {

    private TextView mDeviceName;
    private TextView mDeviceAddress;
    private TextView mStatus;
    private TextView mVisibility;
    private BluetoothDevice mConnectedDevice;
    private TextView mWearDeviceName;
    private TextView mWearDeviceAddress;
    private TextView mWearStatus;
    private Button mConnectWearButton;

    public void setVisibility(String s) {
        mVisibility.setText(s);
    }

    public void setConnectedDevice( BluetoothDevice mConnectedDevice) {
        this.mConnectedDevice = mConnectedDevice;
        mDeviceName.setText(mConnectedDevice.getName());
        mDeviceAddress.setText(mConnectedDevice.getAddress());
        mStatus.setText("CONNECTED");
    }
    public void disconnectedDevice() {
        mDeviceName.setText("null");
        mDeviceAddress.setText("null");
        mStatus.setText("DISCONNECTED");
        mConnectedDevice = null;

    }
    public void changeStatus() {
        if(mStatus.getText().equals("DISCONNECTED"))
            mStatus.setText("CONNECTED");
        else
            mStatus.setText("DISCONNECTED");
    }

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDeviceName = view.findViewById(R.id.device_name);
        mDeviceAddress = view.findViewById(R.id.device_address);
        mStatus = view.findViewById(R.id.connection_status);
        mVisibility = view.findViewById(R.id.bluetooth_visibility);
        
        mWearDeviceName = view.findViewById(R.id.device_name_wear);
        mWearDeviceAddress = view.findViewById(R.id.device_address_wear);
        mWearStatus = view.findViewById(R.id.connection_status_wear);
        //view.findViewById();
    }

    public void setConnectedWearDevice(BluetoothDevice remoteDevice) {
        mWearDeviceName.setText(remoteDevice.getName());
        mWearDeviceAddress.setText(remoteDevice.getAddress());
        mWearStatus.setText("CONNECTED");

    }
}

