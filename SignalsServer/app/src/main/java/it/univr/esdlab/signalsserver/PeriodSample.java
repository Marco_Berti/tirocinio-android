package it.univr.esdlab.signalsserver;

import java.util.LinkedList;

public class PeriodSample {
    private LinkedList<SensorData> accelData;
    private LinkedList<SensorData> linearAccelData;
    private LinkedList<SensorData> gyroData;
    private LinkedList<SensorData> magnetoData;
    private String id;

    public PeriodSample(String id){
        this.id = id;
        this.accelData = new LinkedList<>();
        this.linearAccelData = new LinkedList<>();
        this.gyroData = new LinkedList<>();
        this.magnetoData = new LinkedList<>();
    }

    public int getSize(){
        return accelData.size();
    }

    public void addAccelData(SensorData s){
        accelData.add(s);
    }

    public void addLinAccelData(SensorData s){
        linearAccelData.add(s);
    }
    public void addGyroData(SensorData s){
        gyroData.add(s);
    }
    public void addMagnetData(SensorData s){
        magnetoData.add(s);
    }

    public LinkedList<SensorData> getAccelData() {
        return accelData;
    }

    public LinkedList<SensorData> getLinearAccelData() {
        return linearAccelData;
    }

    public LinkedList<SensorData> getGyroData() {
        return gyroData;
    }

    public LinkedList<SensorData> getMagnetoData() {
        return magnetoData;
    }

    public String getId() {
        return id;
    }
}