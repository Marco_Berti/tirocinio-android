package it.univr.esdlab.signalsserver;

/**
 * Created by stefano on 10/09/2017.
 */

public class SensorData {

    private double X;
    private double Y;
    private double Z;
    private long TimeStamp;

    public SensorData(double x, double y, double z, long timeStamp)
    {
        this.X = x;
        this.Y = y;
        this.Z = z;
        this.TimeStamp = timeStamp;
    }

    public double getX() {
        return X;
    }

    public double getY() {
        return Y;
    }

    public double getZ() {
        return Z;
    }

    public long getTimeStamp() {
        return TimeStamp;
    }

    @Override
    public String toString() {
        return "{X:" + X + "\tY:"+ Y + "\tZ:" + Z + "}";
    }
}
