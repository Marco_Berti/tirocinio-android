package it.univr.esdlab.signalsserver;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import java.util.LinkedList;

import static android.content.Context.SENSOR_SERVICE;

public class SensorDataManager implements SensorEventListener {

    private static final String TAG = "SensorDataManager";
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mLinearAccelerometer;
    private Sensor mGyroscope;
    private Sensor mMagneto;

    private Boolean mRecordingStarted = false;

    private Context mContext;
    private int mSessionId;

    private long startingTime = 0;

    private long mStartAbsTime;
    private long mStopAbsTime;

    private PeriodSample currentPeriodSample;
    private int subsectionsCounter;
    private LinkedList<String> subSectionsIds;

    public SensorDataManager() {
        mSensorManager = null;
        mAccelerometer = null;
        mLinearAccelerometer = null;
        mMagneto = null;
        subsectionsCounter = 0;
        subSectionsIds = new LinkedList<>();
    }

    public void clearData(){
        startingTime = 0;
    }


    public void setStartRecording(int sessionId, boolean recordingState)
    {
        mSessionId = sessionId;

        if(recordingState) {
            subsectionsCounter = 0;
            subSectionsIds.clear();
            startingTime = SystemClock.elapsedRealtime();
            mStartAbsTime = System.currentTimeMillis();
            currentPeriodSample = new PeriodSample(nextSubsectionID());
        }
        mRecordingStarted = recordingState;
        if(!recordingState) {
            mStopAbsTime = System.currentTimeMillis();
            Thread uploaderThread = new SubSectionUploaderThread(currentPeriodSample,mSessionId);
            uploaderThread.start();
            Thread dataUploaderThread = new DataUploaderThread(mStartAbsTime, mStopAbsTime, subSectionsIds, mSessionId);
            dataUploaderThread.start();
            try {
                uploaderThread.join();
                dataUploaderThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            DataMapper.getInstance().startPushReplication(sessionId);
            Handler h = new Handler(Looper.getMainLooper());
            h.post(()->Toast.makeText(mContext, "Replication started", Toast.LENGTH_SHORT).show());
        }
    }

    public void init(Context context) {

        mContext = context;

        mSensorManager = (SensorManager)context.getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mLinearAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mMagneto = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        clearData();
    }


    public void onResume() {
        if(mAccelerometer != null) {
            mSensorManager.registerListener(this, mAccelerometer, android.hardware.SensorManager.SENSOR_DELAY_GAME);
        }
        if(mGyroscope !=  null) {
            mSensorManager.registerListener(this, mGyroscope, android.hardware.SensorManager.SENSOR_DELAY_GAME);
        }
        if(mLinearAccelerometer !=  null) {
            mSensorManager.registerListener(this, mLinearAccelerometer, android.hardware.SensorManager.SENSOR_DELAY_GAME);
        }
        if(mMagneto !=  null) {
            mSensorManager.registerListener(this, mMagneto, android.hardware.SensorManager.SENSOR_DELAY_GAME);
        }
    }


    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {

        if (mRecordingStarted){
            Log.d(TAG, "timestamp: " + (event.timestamp/1000000) + "\nStartingTime: " + startingTime +"\n tot: " +((event.timestamp/1000000)-startingTime));
            SensorData data = new SensorData(event.values[0], event.values[1],event.values[2], (event.timestamp/1000000)-startingTime);
            switch (event.sensor.getType()){
                case Sensor.TYPE_ACCELEROMETER:
                    currentPeriodSample.addAccelData(data);
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    currentPeriodSample.addGyroData(data);
                    break;
                case Sensor.TYPE_LINEAR_ACCELERATION:
                    currentPeriodSample.addLinAccelData(data);
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    currentPeriodSample.addMagnetData(data);
                    break;
                default:
                    break;
            }
            checkSize();
        }
    }

    private void checkSize() {
        if (currentPeriodSample.getSize() >= 10000) {
            new SubSectionUploaderThread(currentPeriodSample, mSessionId).start();
            currentPeriodSample = new PeriodSample(nextSubsectionID());
        }
    }

    private String nextSubsectionID(){
        String newId = mSessionId + "_" + subsectionsCounter++ + "_subsection";
        subSectionsIds.add(newId);
        return newId;
    }

}

