package it.univr.esdlab.signalsserver;

import android.media.MediaPlayer;
import android.util.Log;

import java.sql.Time;
import java.time.Duration;

public class SoundThread extends Thread {

    private static final String TAG = "SoundThread" ;
    private MediaPlayer mp;
    private int duration;
    private int interval;

    public SoundThread(MediaPlayer mp, int duration, int interval) {
        this.mp = mp;
        this.duration = duration;
        this.interval = interval;
    }


    public void run() {

        long startTime = System.currentTimeMillis(); //fetch starting time
        while((System.currentTimeMillis() - startTime) < duration) {
            Log.d(TAG, "BEEP");
            mp.start();
            try {
                sleep(interval/2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //mp.stop();
        }
    }
}
