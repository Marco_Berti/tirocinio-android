package it.univr.esdlab.signalsserver;

import android.os.SystemClock;
import android.util.Log;

public class SubSectionUploaderThread extends Thread {

    private static final String TAG = "UploaderThread";
    private boolean stop;
    private  int sessionID;
    private PeriodSample mPeriodSample;

    public SubSectionUploaderThread(PeriodSample periodSample, int sessionID) {
        this.stop = false;
        this.sessionID = sessionID;
        this.mPeriodSample = periodSample;
    }

    public void run() {
        long time = SystemClock.currentThreadTimeMillis();
        Log.d(TAG, "Sono la thread " + mPeriodSample + ", minchia");
        DataMapper.getInstance().addDataSubSection(sessionID, mPeriodSample);

        Log.d(TAG, "time: " + (SystemClock.currentThreadTimeMillis()-time) );
        Log.d(TAG, "Sono la thread " + mPeriodSample + ", Passo e chiudo!");
    }
}
