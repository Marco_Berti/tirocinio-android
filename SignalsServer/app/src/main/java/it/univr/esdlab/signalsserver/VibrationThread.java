package it.univr.esdlab.signalsserver;

import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

public class VibrationThread extends Thread{

    //private MainActivity mMainActivity;
    private static final String TAG = "VibrationThread";
    private Vibrator mVibrator;
    private long[] pattern;
    private int repeat;

    public VibrationThread(Vibrator mVibrator, long[] pattern, int repeat) {
        this.mVibrator = mVibrator;
        this.pattern = pattern;
        this.repeat = repeat;
    }

    public void run() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int[] amplitudes = new int[pattern.length];
            for(int i = 0; i < amplitudes.length; i+=2) {
                amplitudes[i] = VibrationEffect.DEFAULT_AMPLITUDE;
                amplitudes[i + 1] = 0;
            }
            //Arrays.fill(amplitudes, VibrationEffect.DEFAULT_AMPLITUDE);
            mVibrator.vibrate(VibrationEffect.createWaveform(pattern, amplitudes, repeat));
        }
        else {
            long[] newArray = new long[]{pattern[1], pattern[0]}; //girato array perché il metodo vibrate parte con la durata della pausa e poi quella della vibrazione
            //Log.d(TAG, Arrays.toString(pattern) + "; repeat" + repeat);
            mVibrator.vibrate(newArray, repeat);
        }
    }
}
