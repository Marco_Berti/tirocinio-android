package it.univr.esdlab.signalsserverwear;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

class AcceptThread extends Thread {
    private static final String NAME = "signals";
    private static final String TAG = "AcceptThread";
    private static final UUID MY_UUID = UUID.fromString("b755e5a4-3bd7-4284-b4b9-979e9aba851b");
    private final BluetoothServerSocket mmServerSocket;
    private BluetoothAdapter mBluetoothAdapter;
    private MainActivity mMainActivity;

    public AcceptThread(MainActivity mMainActivity) {
        // Use a temporary object that is later assigned to mmServerSocket
        // because mmServerSocket is final.
        BluetoothServerSocket tmp = null;
        this.mMainActivity = mMainActivity;
        this.mBluetoothAdapter = mMainActivity.getBluetoothAdapter();
        try {
            // MY_UUID is the app's UUID string, also used by the client code.
            tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
        } catch (IOException e) {
            Log.e(TAG, "Socket's listen() method failed", e);
        }
        mmServerSocket = tmp;
    }

    public void run() {
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned.
        Log.d(TAG, "Thread started");
        while (true) {
            try {
                socket = mmServerSocket.accept();
            } catch (IOException e) {
                Log.e(TAG, "Socket's accept() method failed", e);
                break;
            }

            if (socket != null) {
                // A connection was accepted. Perform work associated with
                // the connection in a separate thread.
                manageMyConnectedSocket(socket);
                try {
                    mmServerSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    // Closes the connect socket and causes the thread to finish.

    public void cancel() {
        try {
            mmServerSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Could not close the connect socket", e);
        }
    }

    private void manageMyConnectedSocket(BluetoothSocket socket) {
        Log.d(TAG, "CONNESSIONE STABILITA");
        ConnectedThread ct = new ConnectedThread(socket, mMainActivity);
        ct.start();
        //DataStorage.getInstance().setSocket(null);
        Handler myHandler = new Handler(Looper.getMainLooper());
        myHandler.post(()-> mMainActivity.setConnected(true));
        //mMainActivity.setConnectedThread(ct);

    }
}
