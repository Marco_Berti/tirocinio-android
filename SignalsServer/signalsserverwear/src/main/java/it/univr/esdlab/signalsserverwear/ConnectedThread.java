package it.univr.esdlab.signalsserverwear;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ConnectedThread extends Thread {
    private static final String TAG = "Connected Thread";
    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
    private byte[] mmBuffer; // mmBuffer store for the stream
    private MainActivity mMainActivity;

    public ConnectedThread(BluetoothSocket socket, MainActivity mainActivity) {
        mMainActivity = mainActivity;
        mmSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the input and output streams; using temp objects because
        // member streams are final.
        try {
            tmpIn = socket.getInputStream();
        } catch (IOException e) {
            Log.e(TAG, "Error occurred when creating input stream", e);
        }
        try {
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            Log.e(TAG, "Error occurred when creating output stream", e);
        }

        mmInStream = tmpIn;
        mmOutStream = tmpOut;
    }

    public void run() {
        //mmBuffer = new byte[1024];
        int numBytes; // bytes returned from read()

        // Keep listening to the InputStream until an exception occurs.
        while (mmSocket.isConnected()) {
            try {
                mmBuffer = new byte[1024];
                // Read from the InputStream.
                numBytes = mmInStream.read(mmBuffer);
                Log.d(TAG, new String(mmBuffer).trim());
                mMainActivity.readMessage((new String(mmBuffer).trim()));


                // Send the obtained bytes to the UI activity.
            } catch (IOException e) {
                Log.d(TAG, "Input stream was disconnected", e);
                break;
            }
        }
        try {
            mmSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Handler myHandler = new Handler(Looper.getMainLooper());
        myHandler.post(() -> {
            mMainActivity.setConnected(false);
        });
    }

    // Call this from the main activity to send data to the remote device.
    public void write(byte[] bytes) {
        try {
            mmOutStream.write(bytes);
            Log.e(TAG, new String(bytes));

            // Share the sent message with the UI activity.
        } catch (IOException e) {
            Log.e(TAG, "Error occurred when sending data", e);

            // Send a failure message back to the activity.
        }
    }

    // Call this method from the main activity to shut down the connection.
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Could not close the connect socket", e);
        }
    }
}
