package it.univr.esdlab.signalsserverwear;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

public class MainActivity extends WearableActivity {

    private static final String TAG = "MainActivity";
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_DISCOVERY = 2;
    private TextView mTextView;
    private BluetoothAdapter mBluetoothAdapter;
    private Vibrator mVibrator;
    private AcceptThread mAcceptThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Log.d(TAG, mTextView.toString());
        mTextView = findViewById(R.id.testo_wear);
        Log.d(TAG, mTextView.toString());


        // Enables Always-on
        setAmbientEnabled();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //if (mConnectedThread == null)
            initialize();

    }

    private void initialize() {
        Log.i(TAG, "initialize");

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter);

        if (mBluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            Toast.makeText(this, "Bluetooth is not supported", Toast.LENGTH_SHORT).show();
        }
        else
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        else
        if(mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            //rende il dispositivo visibile per 300 secondi
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivityForResult(discoverableIntent, REQUEST_DISCOVERY);
        }
        else{
            startThread();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);

        if(requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
            //rende il dispositivo visibile per 300 secondi
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivityForResult(discoverableIntent, REQUEST_DISCOVERY);
        }
        if (requestCode == REQUEST_DISCOVERY && resultCode == 300) {
                Toast.makeText(this, "Bluetooth enabled, Start thread", Toast.LENGTH_SHORT).show();
                startThread();
        }
        else
            Toast.makeText(this, "Not visible", Toast.LENGTH_SHORT).show();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceive");
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                Log.d(TAG, "Connessione stabilita");
                mTextView.setText("CONNESSO");
            }
            else if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)){
                Log.d(TAG, "Connessione chiusa");
                mTextView.setText("DISCONNESSO");
            }
        }
    };

    void startThread() {
        mAcceptThread = new AcceptThread(this);
        mAcceptThread.start();
    }

    public BluetoothAdapter getBluetoothAdapter() {
        return mBluetoothAdapter;
    }

    public void readMessage(String message) {
        Log.d(TAG, "MESSAGGIO RICEVUTO: " + message);
        if(message.equals("END"))
            mVibrator.cancel();
        else{
            String[] s = message.split(",");
            int duration = Integer.parseInt(s[1]);
            int interval = Integer.parseInt(s[2]);
            duration = duration * 1000;
            boolean manual = false;
            if (s[3].equals("manual"))
                manual = true;

            if (s[0].equals("vibration")) {
                if (interval != 0) {
                    int pulseNumber = duration / interval;
                    long[] pattern = new long[pulseNumber * 2];
                    for (int i = 0; i < pulseNumber * 2; i += 2) {
                        pattern[i] = interval / 2;
                        pattern[i + 1] = interval / 2;
                    }
                    if (!manual) {
                        //Log.d(TAG, "pattern" + Arrays.toString(pattern));
                        //discontinuousVibration(pattern, -1);
                        VibrationThread vt = new VibrationThread(mVibrator, pattern, -1);
                        vt.start();

                    } else {
                        //discontinuousVibration(pattern, 0);
                        long[] freePattern = {100000, 0};//è brutto ma funziona
                        Log.d(TAG, "pattern" + Arrays.toString(freePattern));
                        VibrationThread vt = new VibrationThread(mVibrator, freePattern, 0);
                        vt.start();
                    }
                } else {
                    long[] pattern = {100000, 0};//è brutto ma funziona
                    Log.d(TAG, "pattern" + Arrays.toString(pattern));
                    //discontinuousVibration(pattern, 0);
                    VibrationThread vt = new VibrationThread(mVibrator, pattern, 0);
                    vt.start();
                }
            }
        }
    }

    public void setConnected(boolean b) {
        if(b)
            mTextView.setText("CONNECTED");
        else {
            mTextView.setText("DISCONNECTED");
            startThread();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }
}
